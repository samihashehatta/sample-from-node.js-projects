const channel = require('./channel');

const jobs = require('../Jobs/jobify');

const {cantAssertQueue,cantOpenChannel} = require('../Messages/errors');

const queue = "testing";

const listen = () => {

    channel.then((ch) => {

        ch.assertQueue(queue,{ durable: true })
        
        .then((ok) => {
            ch.bindQueue(queue,'mona','mona.why')
            .then(ex =>{
                ch.consume(queue,(job) => {
                    console.log('heeeeeeeeeeeeeeeeeeey',job.content.toString())},
                  {
                      noAck: false
                  });
             }).catch((error) => cantAssertQueue(error))
            })
        .catch((error) => cantAssertQueue(error)
        );
    
    }).catch((error) => cantOpenChannel(error));  
}

module.exports = {listen};
