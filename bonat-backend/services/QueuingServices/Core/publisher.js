const channel = require('./channel');

const queue = "DefaultQueue";

const {cantAssertQueue,cantOpenChannel} = require('../Messages/errors');

const dispatch = (job) => {

  channel.then( (ch) => {

    ch.assertQueue(queue,{ durable: true })
  
    .then((ok) => {

      return ch.sendToQueue(queue,

      Buffer.from(JSON.stringify(job)),

      { persistent: true});
      
    })
  
    .catch((error)=>cantAssertQueue(error));
  
  })

  .catch((error)=>cantOpenChannel(error));
  
}

module.exports = {dispatch};


// const channel = require('./channel');

// const queue = "sawsan";

// const {cantAssertQueue,cantOpenChannel} = require('../Messages/errors');

// const dispatch = (job) => {

//   channel.then( (ch) => {
//     ch.assertExchange('mona','topic',{autoDelete:false})
//     .then(exchange=>{
      
//       console.log(exchange,job)
//       exchange.publish('mona','mona.why',Buffer.from(JSON.stringify(job)))
//       .then(ok=>{
//         ch.assertQueue(queue)
//         .then(queue=>{
//           ch.bindQueue(queue,'mona','mona.why').then(ok=>{
//             console.log(ok)

//           })
//         })
//       })
//     })
//     .catch((error)=>cantAssertQueue(error));
  
//   })

//   .catch((error)=>cantOpenChannel(error));
  
// }

// // var amqp = require('amqp');
// // var connection = amqp.createConnection({ host: 'your host', port: 'port' });

// // connection.on('ready',function(){
// //     connection.exchange('exchangeName', options={type:'topic', autoDelete:false}, function(exchange){
// //         console.log('start send message');
// //         exchange.publish('1routingKey','hello world');
// //     });
// // });
// module.exports = {dispatch};