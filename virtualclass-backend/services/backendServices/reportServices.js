const { BBBMongo } = require('../../db/mongoose');
const _ = require('lodash');
const moment = require('moment');
const { duration } = require('moment');
const getQuiz = async (reqQuery) => {
  try {
    const query = _.pick(reqQuery, ['userName', 'meetingId']);

    if (!query.userName || !query.meetingId) throw 'data is not complete';

    const polls = await (await BBBMongo).db.collection('polls').find({ question: false, meetingId: query.meetingId, 'responses.userName': query.userName }).toArray();
    const modify = polls.map(item => {
      const returnValue = {
        _id: item._id,
        question: item.questionString,
        meetingId: item.meetingId,
        userName: query.userName,
        grade: item.responses ? item.responses.filter(ele => ele.userName == query.userName)[0].grade : 0
      };
      return returnValue;
    });
    return (modify);
  } catch (err) {
    return err;
  }
};
const diffBetweenDates = (date1, date2) => {
  const data1Moment = new moment(date1);

  const data2Moment = new moment(date2);

  return data2Moment.diff(data1Moment, 'seconds');
};
const calcWastedTime = (meeting, user) => {
  const wastedTime = Math.abs(diffBetweenDates(meeting.durationProps.createdDate, user.loginTime));
  const wastedAtEnd = moment(user.logoutTime).isSameOrAfter(meeting.endTime) ? 0 : Math.abs(diffBetweenDates(meeting.durationProps.createdDate, user.loginTime));
  return wastedTime + wastedAtEnd;
};
const getMeetings = async (meetingId) => {
  try {
    const meeting = await (await BBBMongo).db.collection('meetings').findOne({ meetingId });
    const users = await (await BBBMongo).db.collection('users').find({ meetingId }).toArray();
    if (meeting.meetingEnded) {
      const meetingDuration = Math.abs(diffBetweenDates(meeting.endTime, meeting.durationProps.createdDate));
      const attendee = users.map(user => {
        return {
          userName: user.name,
          userId: user.userId,
          loginTime: new moment(user.loginTime),
          logoutTime: new moment(user.logoutTime),
          wastedTime: calcWastedTime(meeting, user, meetingDuration),
          userDeviceType: user.userDeviceType,
          userRegion: user.userRegion
        };
      });
      const returndValue = {
        meeting: {
          meetingId,
          meetingName: meeting.name,
          createDate: meeting.durationProps.createdDate,
          endDate: meeting.endTime,
          duration: meetingDuration

        },
        attendee
      };

      return (returndValue);
    }
    else return 'meeting hasn\'t ended yet';

  }
  catch (err) {
    console.log(err);
    return (err);
  }
};
module.exports = {
  getQuiz,
  getMeetings
};