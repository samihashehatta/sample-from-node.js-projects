const services = require('../services/backendServices/reportServices');

const getQuizGrade = (req, res) => {
  services.getQuiz(req.query)
    .then(grades => res.send(grades))
    .catch(err => res.status(400).send(err));
};

const getMeetingsReport = (req, res) => {
  services.getMeetings(req.params.id)
    .then(grades => res.send(grades))
    .catch(err => res.status(400).send(err));
};

module.exports = {
  getQuizGrade,
  getMeetingsReport
};