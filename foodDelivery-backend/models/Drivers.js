const mongoose = require('mongoose');
const validator = require('validator');
const _ = require('lodash');
const passportMongoose = require('passport-local-mongoose')

let DriverSchema = new mongoose.Schema({
	name: {
		type: String,
		required: false,
		minlenght: 2,
		trim: true
	},
	city: {
		type: String,
		required: false,
		minlenght: 2,
		trim: true
    },
    drivingId: {
		type: String,
		required: false,
		minlenght: 2,
		trim: true
    },
    imgUrl: {
		type: String,
		required: false,
		minlenght: 2,
		trim: true
	},
	email: {
		type: String,
		required: false,
		minlenght: 2,
		trim: true
    },
	phoneNumber: {
		type: String,
		required: false,
		trim: true

	},
	type: {
		type: String,
		required: false,
		trim: true,
		default:'driver'

	},
	car: {
		type: Boolean,
		required: false,
		trim: true
	},
	student: {
		type: Boolean,
		required: false,
		trim: true
	},
    updatedAt: {
		type: Date,
		required: false,
		default: Date.now
	},
	createdAt: {
		type: Date,
		required: false,
		default: Date.now
	}
});

DriverSchema.methods.validPassword =  function(password) {
	let rest = this
	return new Promise((resolve,reject)=>{
		rest.authenticate(password)
		.then(response=>{
			resolve(response.user)
		})
		.catch(err=>reject(null))
	})
	
  };

DriverSchema.methods.toJSON = function(){
	let Drivers = this;
	let DriversObject = Drivers.toObject();
	return _.pick(DriversObject, ['_id',
				'city',
                'name',
                'drivingId',
                'imgUrl',
                'email',
                'car',
                'student',
                'phoneNumber',
                'createdAt',
                'updatedAt']);
}
mongoose.plugin(passportMongoose)


let Drivers = mongoose.model('Drivers', DriverSchema);

module.exports = {Drivers}