const mongoose = require('mongoose');
const passportMongoose = require('passport-local-mongoose')
const validator = require('validator');
const bcrypt = require('bcryptjs')
const _ = require('lodash');
const crypto = require('crypto')

let RestaurantSchema = new mongoose.Schema({
	name: {
		type: String,
		required: false,
		minlenght: 2,
		trim: true
    },
    email: {
		type: String,
		required: false,
		minlenght: 2,
		trim: true
    },
    location: {
		type: Array,
		required: false,
		minlenght: 2,
		trim: true
    },
    imgUrl: {
		type: String,
		required: false,
		minlenght: 2,
		trim: true
	},
	cover: {
		type: String,
		required: false,
		minlenght: 2,
		trim: true
    },
    city: {
		type: mongoose.Schema.Types.ObjectId,
		required:false,
		ref: 'Locations',
    },
    cuisine: {
		type: String,
		required: false,
		minlenght: 2,
		trim: true
	},
	phoneNumber: {
		type: String,
		required: false,
		trim: true
	},
	opensAt: {
		type: String,
		required: false,
		trim: true
	},
	password: {
		type: String,
		required: false,
		trim: true
    },
    menu:[{
		type: mongoose.Schema.Types.ObjectId,
		required:false,
		ref: 'Products',
		//unique:true
	}],
	deliveryFee:{
		type: Number,
        required: false,
	},
	delivery:{
		type: Boolean,
        required: false,
	},
	takeAway:{
		type: Boolean,
        required: false,
	},
	type: {
		type: String,
		required: false,
		trim: true,
		default:'restaurant'

	},
	salt:{
		type: String,
		required: false,
	},
	hash:{
		type: String,
		required: false,
	},
    updatedAt: {
		type: Date,
		required: false,
		default: Date.now
	},
	createdAt: {
		type: Date,
		required: false,
		default: Date.now
	}
});

RestaurantSchema.methods.validPassword =  function(password) {
	let rest = this
	return new Promise((resolve,reject)=>{
		rest.authenticate(password)
		.then(response=>{
			resolve(response.user)
		})
		.catch(err=>reject(null))
	})
	
  };

// RestaurantSchema.pre('save',function(next){
// 	let user = this;

//     // generate a salt
//     bcrypt.genSalt(10, function(err, salt) {
//         if (err) return next(err);

//         // hash the password using our new salt
//         bcrypt.hash(user.password, salt, function(err, hash) {
//             if (err) return next(err);

//             // override the cleartext password with the hashed one
//             user.password = hash;
//             next();
//         });
// 	});
	

// })

RestaurantSchema.methods.toJSON = function(){
	let Restaurants = this;
	let RestaurantsObject = Restaurants.toObject();
    return _.pick(RestaurantsObject, ['_id',
                'name',
                'email',
                'location',
                'imgUrl',
                'cuisine',
                'city',
				'phoneNumber',
				'deliveryFee',
				'delivery',
				'takeAway',
                'opensAt',
				'menu',
				'salt',
				'hash',
                'createdAt',
                'updatedAt']);
}
mongoose.plugin(passportMongoose)

let Restaurants = mongoose.model('Restaurants', RestaurantSchema);

module.exports = {Restaurants}