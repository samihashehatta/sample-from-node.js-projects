const mongoose = require('mongoose');
const validator = require('validator');
const _ = require('lodash');

let OrderSchema = new mongoose.Schema({
    driverId:{
		type: mongoose.Schema.Types.ObjectId,
		required:false,
		ref: 'Drivers'
    },
    restaurantId:[{
		type: mongoose.Schema.Types.ObjectId,
		required:false,
		ref: 'Restaurants'
    }],
    customerId:{
		type: mongoose.Schema.Types.ObjectId,
		required:false,
		ref: 'Customers'
    },
    transactionId:{
		type: mongoose.Schema.Types.ObjectId,
		required:false,
		ref: 'Transactions'
	},
    location: {
		type: String,
		required: false,
		minlenght: 2,
		trim: true
    },
    vat: {
		type: Number,
		required: false,
		minlenght: 2,
		trim: true
	},
    amount: {
		type: Number,
		required: false,
		minlenght: 2,
		trim: true
	},
	total: {
		type: Number,
		required: false,
		trim: true
	},
	date: {
		type: String,
		required: false,
		trim: true
	},
	status: {
        type: String,
        required: false,
        enum: ['pending','active','done','canceled','delivering','processing'],
        default:'pending'
	},
	delivery:{
		type: Boolean,
        required: false,
	},
	deliveryFee:{
		type: Number,
        required: false,
	},
    items:	{
        type: Array,
		required: false,
	},
	time: {
		type: String,
		required: false,
	},
    updatedAt: {
		type: Date,
		required: false,
		default: Date.now
	},
	createdAt: {
		type: Date,
		required: false,
		default: Date.now
	},
	username: {
		type: String,
		required: false,
		unique:false,

	}
});

OrderSchema.methods.toJSON = function(){
	let Orders = this;
	let OrdersObject = Orders.toObject();
    return _.pick(OrdersObject, ['_id',
                'driverId',
                'location',
                'imgUrl',
                'restaurantId',
                'customerId',
                'transactionId',
                'vat',
                'amount',
                'total',
                'status',
                'items',
				'date',
				'delivery',
				'deliveryFee',
                'createdAt',
                'updatedAt']);
}


let Orders = mongoose.model('Orders', OrderSchema);

module.exports = {Orders}