const mongoose = require('mongoose');
const validator = require('validator');
const _ = require('lodash');
const passportMongoose = require('passport-local-mongoose')

let CustomerSchema = new mongoose.Schema({
	name: {
		type: String,
		required: false,
		minlenght: 2,
		trim: true
    },
    location: {
		type: String,
		required: false,
		minlenght: 2,
		trim: true
	},
	city: {
		type: String,
		required: false,
		minlenght: 2,
		trim: true
    },
    imgUrl: {
		type: String,
		required: false,
		minlenght: 2,
		trim: true
	},
	email: {
		type: String,
		required: false,
		minlenght: 2,
		trim: true
    },
	phoneNumber: {
		type: String,
		required: false,
		trim: true
	},
	googleId: {
		type: String,
		required: false,
		trim: true
	},
	facebookId: {
		type: String,
		required: false,
		trim: true
	},
    updatedAt: {
		type: Date,
		required: false,
		default: Date.now
	},
	createdAt: {
		type: Date,
		required: false,
		default: Date.now
	},
	type: {
		type: String,
		required: false,
		trim: true,
		default:'customer'

	},
});

CustomerSchema.methods.validPassword =  function(password) {
	let rest = this
	return new Promise((resolve,reject)=>{
		rest.authenticate(password)
		.then(response=>{
			resolve(response.user)
		})
		.catch(err=>reject(null))
	})
	
  };
CustomerSchema.methods.toJSON = function(){
	let Customers = this;
	let CustomersObject = Customers.toObject();
    return _.pick(CustomersObject, ['_id',
                'name',
                'location',
                'imgUrl',
                'email',
                'phoneNumber',
                'createdAt',
                'updatedAt']);
}
mongoose.plugin(passportMongoose)


let Customers = mongoose.model('Customers', CustomerSchema);

module.exports = {Customers}