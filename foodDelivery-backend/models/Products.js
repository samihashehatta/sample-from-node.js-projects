const mongoose = require('mongoose');
const validator = require('validator');
const _ = require('lodash');
const uuid = require('uuid/v4')

let ProductSchema = new mongoose.Schema({
	restaurantId:{
		type: mongoose.Schema.Types.ObjectId,
		required:false,
		ref: 'Restaurants'
	},
	name: {
		type: String,
		required: false,
		minlenght: 2,
		trim: true
	},
	options: {
		type: Array,
		required: false,
		minlenght: 2,
		trim: true
    },
	category: {
		type: String,
		required: false,
		minlenght: 2,
		trim: true
    },
    imgUrl: {
		type: String,
		required: false,
		minlenght: 2,
		trim: true
	},
	description: {
		type: String,
		required: false,
		minlenght: 2,
		trim: true
    },
	price: {
		type: Number,
		required: false,
		trim: true
	},
    updatedAt: {
		type: Date,
		required: false,
		default: Date.now
	},
	createdAt: {
		type: Date,
		required: false,
		default: Date.now
	},
	username: {
		type: String,
		required: false,
		unique:false,

	},
});

ProductSchema.methods.toJSON = function(){
	let Products = this;
	let ProductsObject = Products.toObject();
	return _.pick(ProductsObject, ['_id',
				'category',
				'options',
				'restaurantId',
				'description',
                'name',
                'imgUrl',
                'price',
                'createdAt',
                'updatedAt']);
}


let Products = mongoose.model('Products', ProductSchema);

module.exports = {Products}