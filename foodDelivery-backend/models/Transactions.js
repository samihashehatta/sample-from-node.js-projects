const mongoose = require('mongoose');
const validator = require('validator');
const _ = require('lodash');

let TransactionSchema = new mongoose.Schema({
    orderId:{
		type: mongoose.Schema.Types.ObjectId,
		required:false,
		ref: 'Orders'
    },
    customerId:{
		type: mongoose.Schema.Types.ObjectId,
		required:false,
		ref: 'Customers'
    },
    stripeTransactionId:{
		type: String,
		required:false,
	},
    vat: {
		type: Number,
		required:false,
		minlenght: 2,
		trim: true
	},
    amount: {
		type: Number,
		required:false,
		minlenght: 2,
		trim: true
	},
	total: {
		type: Number,
		required:false,
		trim: true
	},
	date: {
		type: Date,
		required:false,
		trim: true
	},
	status: {
        type: String,
        required:false,
        enum: ['pending','active','done'],
        default:'pending'
    },
    updatedAt: {
		type: Date,
		required:false,
		default: Date.now
	},
	createdAt: {
		type: Date,
		required:false,
		default: Date.now
	},
	username: {
		type: String,
		required: false,
		unique:false,

	}
});

TransactionSchema.methods.toJSON = function(){
	let Transactions = this;
	let TransactionsObject = Transactions.toObject();
    return _.pick(TransactionsObject, ['_id',
                'customerId',
                'stripeTransactionId',
                'vat',
                'amount',
                'total',
                'status',
                'date',
                'createdAt',
                'updatedAt']);
}


let Transactions = mongoose.model('Transactions', TransactionSchema);

module.exports = {Transactions}