const mongoose = require('mongoose');
const passport = require('passport');
const LocalStrategy = require('passport-local');

const {Drivers} =require('../models/Drivers')

module.exports = (passport)=>{
    passport.use('local-driver',new LocalStrategy({usernameField: 'email',
    passwordField: 'password'},
    function(email, password, done) {
        Drivers.findOne({ email }, function (err, user) {
      if (err) {
        return done(err); }
      if (!user) {

        return done(null, false, { message: 'Incorrect username.' });
      }
     else{
      user.authenticate(password).then(response=>{
              if(response.user)return done(null, response.user);
              else return done(null, false);
          })
          .catch(err=>{
              return done(null, false);

          })
        }
      });
}))
}
