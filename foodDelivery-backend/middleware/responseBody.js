const generalResponse =  (data = {},validationErrors = [], debugError = '', message = '') => {

    return {
        data,
        debugError,
        validationErrors,
        message,
    }
}

module.exports =  {generalResponse}