
const passport = require('passport')

const authenticate = (req,res,next)=>{
    if(req.isAuthenticated())
    {
        next()
    }
    else res.redirect('/')
}


const driver = (req,res,next)=>{
    if(req.user.type == "driver")
    {
        next()
    }
    else res.redirect('/')

}


const restaurant = (req,res,next)=>{
    if(req.user.type == "restaurant")
    {
        next()
    }
    else res.redirect('/')

}

const customer = (req,res,next)=>{
    if(req.user.type == "customer")
    {
        next()
    }
    else res.redirect('/')

}
module.exports=
{
    authenticate,
    driver,
    customer,
    restaurant
}