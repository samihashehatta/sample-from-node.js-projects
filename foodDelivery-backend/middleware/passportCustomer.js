const mongoose = require('mongoose');
const passport = require('passport');
const LocalStrategy = require('passport-local');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;

const {Customers} =require('../models/Customers')

module.exports = (passport)=>{
    passport.use('local-customers',new LocalStrategy(
      {usernameField: 'email',
      passwordField: 'password'},
    function(email, password, done) {
      Customers.findOne({ email }, function (err, user) {
          
      if (err) {

        return done(err); }
      if (!user) {
        return done(null, false);
      }
     else{
      user.authenticate(password).then(response=>{
              if(response.user)return done(null, response.user);
              else return done(null, false);
          })
          .catch(err=>{
              return done(null, false);

          })
        }
      });
    }))

    passport.use('google',new GoogleStrategy({
      clientID:process.env.GOOGLE_CLIENT_ID,
      clientSecret:process.env.GOOGLE_CLIENT_SECRET,
      callbackURL: "/callback"
    },
    function(accessToken, refreshToken, profile, cb) {
      Customers.findOne({ googleId: profile.id })
      .then(user=>{
        if(user) return cb(null,user)
        const newCust = new Customers({
          name:profile.displayName,
          email:profile.emails[0].value,
          googleId:profile.id
        })
        newCust.save().then(user=>{
          return cb(null,user)
        })
        .catch(err=>cb(null,false))
      })
      .catch(err=>cb(null,false))
      ;
    }
  ));

passport.use('facebook',new FacebookStrategy({
        clientID: process.env.FACEBOOK_APP_ID,
        clientSecret: process.env.FACEBOOK_APP_SECRET,
        callbackURL: "/facebook/callback",
        profileFields:["emails","displayName"]
      },
      function(accessToken, refreshToken, profile, cb) {
        Customers.findOne({ facebookId: profile.id })
        .then(user=>{
          if(user) return cb(null,user)
          const newCust = new Customers({
            name:profile.displayName,
            email:profile.emails[0].value,
            facebookId:profile.id
          })
          newCust.save().then(user=>{
            return cb(null,user)
          })
          .catch(err=>{
            cb(err,false)})
        })
        .catch(err=>{
          cb(err,false)})
        ;
      }
));

}
