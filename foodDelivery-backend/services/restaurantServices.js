const { Restaurants } = require('../models/Restaurants');
const { Locations } = require('../models/Location');
const { Orders } = require('../models/Orders');
const { Drivers } = require('../models/Drivers');
const { Products } = require('../models/Products');
const bcrypt = require('bcryptjs')

const creatRestuarnt = (body) =>{
    return new Promise((resolve,reject)=>{
        const newRestt = new Restaurants({
            username:body.email,
            email:body.email,
            name:body.name,
            location:body.location,
            imgUrl:body.imgUrl,
            city:body.city,
            cuisine:body.cuisine,
            phoneNumber:body.phoneNumber,
            opensAt:body.opensAt,
            })
       Restaurants.register(newRestt,body.password)
       .then(res=>resolve(res))
       .catch(err=>reject(err))

    })
}

const loginAuth = (body) =>{
    return new Promise((resolve,reject)=>{
        Restaurants.findOne({email:body.email})
        .then(found=>{

                bcrypt.compare(body.password,found.password)
                .then(res=>{
                    if(res)
                    resolve(found)
                    else reject('not true')
                })
                .catch(err=>reject(err))
        })
        .catch(err=>reject(err))

    })
  
}
const getAllRests = async()=>{
    let cuisines = await Restaurants.find().distinct('cuisine')
    let readified={}        
        for(let cuisine of cuisines){
                let found= await Restaurants.find({cuisine})
                readified[cuisine] = found.length

        }
    console.log(readified)
    return readified

}
const getAll =async (req)=>{
    let city ,q={}, loc={}
    if(req.user&&req.user.type=='customer' &&req.user.city){
        city = req.user.city
        loc = await Locations.findOne({city})
        q.city=loc.id
    }
    let cuisines = await getAllRests()
    let rests = await  Restaurants.find(q).populate('city')
    return {rests,city:loc,cuisines}
}
const search =async (req)=>{
    let city=req.query.city
    let loc = await Locations.findOne({city})
    let cuisines = await getAllRests()

    let rests = await  Restaurants.find({city:loc.id}).populate('city')
    return {rests,city:loc,cuisines}
}
const filtered = async(req)=>{
    let rest 
    if(req.body.cos == 'All') rest = await Restaurants.find()
    else rest = await Restaurants.find({cuisine:req.body.cos})
    let {rests,city,cuisines} = await getAll(req)
    return {rests:rest,city,cuisines,choosen:req.body.cos}
}
const findRestOrders = async(user,status)=>{
    const products = user.menu
    const orders = await Orders.find(
        {
            restaurantId:user.id,
            status:status,
           
        }
    )
    let city = await Locations.findOne({_id:user.city})
    let drivers = await Drivers.find({city:city.city})
    orders.map(obj=>{

       let items =  obj.items.filter((obj)=>{
          return user.menu.includes(obj.id.trim())
        })
       obj.items=items

    })
    return {orders,drivers}
}

const filter = (req,res)=>{
    
}
module.exports={
    creatRestuarnt,
    loginAuth,
    getAll,
    search,
    findRestOrders,
    filtered
}