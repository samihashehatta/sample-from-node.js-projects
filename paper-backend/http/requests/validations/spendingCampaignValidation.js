
const { validator } = require('../../middlewares/validateRequest');
const { check } = require('express-validator');

const validateCreateSpendingCampaign = [
    // company_id','user_id','is_owner','verified
    check('tier_spending_rule_id').not().isEmpty()
    .withMessage("please fill tier_spending_rule_id field with a value"),

    check('name').not().isEmpty()
    .withMessage("please fill name field with a value"),
    
    check('start_date').not().isEmpty()
    .if((value,{req})=>!req.body.test_mode)
    .isAfter(`${new Date()}`)
    .withMessage("please fill start_date with a vaild date"),

    check('end_date').not().isEmpty()
    .if((value,{req})=>!req.body.test_mode)
    .isAfter(`${new Date()}`).custom((value,{req})=>{
        if(new Date(req.body.start_date) <= new Date(value)) return true
        else return false
    })
    .withMessage("please fill end_date with a vaild date"),
 

    check('fixed_point_spent')
    .if((value,{req})=>value)
    .isNumeric().custom((value,{req})=>{
        if(Math.sign(value) == -1) return false
        else return true
    })
    .withMessage("please fill fixed_point_spent with a vaild value"),


    check('global_daily_limit')
    .if((value,{req})=>value)
    .isNumeric().custom((value,{req})=>{
        if(Math.sign(value) == -1) return false
        else return true
    })
    .withMessage("please fill global_daily_limit with a vaild value"),
    validator

]


module.exports = { validateCreateSpendingCampaign };
