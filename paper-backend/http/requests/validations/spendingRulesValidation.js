const { validator } = require('../../middlewares/validateRequest');
const { check } = require('express-validator');

const validateCreateSpendingRule = [
   
    check('code').not().isEmpty().isLength({ min: 3 })

    .withMessage("please fill code field with a value"),

    check('name').not().isEmpty().isLength({ min: 3 })
    .withMessage("please fill name field with a value"),
   
    validator

]

module.exports = { validateCreateSpendingRule };