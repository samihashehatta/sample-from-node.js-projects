const { validator } = require('../../middlewares/validateRequest');
const { check } = require('express-validator');

const validateCreateUser = [
   
    check('email').not().isEmpty().isEmail()
    .withMessage("please fill email field with a value"),

    check('name').not().isEmpty().isLength({ min: 3 })
    .withMessage("please fill name field with a value"),
    
    check('password').not().isEmpty().isLength({ min: 6})
    .withMessage("please fill password field with at least 8 characters"),

    check('type').not().isEmpty().isIn(['admin','normal'])
    .withMessage("please fill type with valid value"),
    check('status').not().isEmpty().isIn(['active','suspended'])
    .withMessage("please fill status with valid value"),
    validator

]

module.exports = { validateCreateUser };