
const { validator } = require('../../middlewares/validateRequest');
const { check } = require('express-validator');
const moment = require('moment')
const addAVoucherValidator = [

    check('spending_rule_id').not().isEmpty().isUUID()
    .withMessage('please fill spending_rule_id with valid value'),
       
    check('start_date').not().isEmpty()
    .if((value,{req})=>!req.body.test_mode)
    .isAfter(`${new Date()}`)
    .withMessage("please fill start_date with a vaild date"),

    check('expiry_date').not().isEmpty()
    .if((value,{req})=>!req.body.test_mode)
    .isAfter(`${new Date()}`).custom((value,{req})=>{
        if(new Date(req.body.start_date) <= new Date(value)) return true
        else return false
    })
    .withMessage("please fill expiry_date with a vaild date"),

    check('expiry_length')
    .if((value,{req})=>req.body.voucher_type == 'system')
    .isEmpty()
    .withMessage('expiry_length can not be set for system vouchers'),
    
    check('expiry_length')
    .if((value,{req})=>req.body.voucher_type == 'point')
    .not().isEmpty().isNumeric()
    .withMessage("please fill expiry_length with a vaild value"),
 
    check('currency').not().isEmpty().isLength({max:4})
    .withMessage('please fill currency with valid value '),

    check('banner_url')
    .if((value,{req})=>value)
    .not().isEmpty().isURL()
    .withMessage("please fill banner_url field with a value"),

    check('terms')
    .if((value,{req})=>value)
    .not().isEmpty().isString()
    .withMessage("please fill company_id field with a value"),

    check('voucher_type')
    .not().isEmpty().isString().isIn(['system','point'])
    .withMessage("please fill voucher_type field with a value"),

    check('min_transaction_value')
    .if((value,{req})=>value)
    .isNumeric()
    .withMessage('please fill min_transaction_value with valid value'),

    check('redemption_point_amount')
    .if((value,{req})=>(req.body.voucher_type == 'system'))
    .isEmpty()
    .withMessage('redemption_point_amount can not be set for system vouchers'),

    check('redemption_point_amount')
    .if((value,{req})=>req.body.voucher_type == 'point')
    .not().isEmpty().isNumeric().custom((value,{req})=>{
        if(Math.sign(value) == -1) return false
        else return true
    })
    .withMessage('redemption_point_amount Must be set for point vouchers and with positive value '),

    check('fixed_voucher_amount')
    .if((value,{req})=>(req.body.pct_voucher_discount))
    .isEmpty()
    .withMessage('You can only fill one of fixed_voucher_amount or pct_voucher_discount'),
    check('fixed_voucher_amount')
    .if((value,{req})=>(!req.body.pct_voucher_discount))
    .not().isEmpty()
    .withMessage('You need to fill fixed_voucher_amount or pct_voucher_discount'),

    check('pct_voucher_discount')
    .if((value,{req})=> req.body.fixed_voucher_amount)
    .isEmpty()
    .withMessage('You can only fill one of fixed_voucher_amount or pct_voucher_discount'),
    check('pct_voucher_discount')
    .if((value,{req})=>!req.body.fixed_voucher_amount)
    .not().isEmpty()
    .withMessage('You need to fill fixed_voucher_amount or pct_voucher_discount'),

    
    check('periodic_limit')
    .if((value,{req})=>req.body.periodic_limit_length)
    .isNumeric()
    .withMessage('You have to fill both Periodic Limit and Periodic Limit length'),

    check('periodic_limit_length')
    .if((value,{req})=>req.body.periodic_limit)
    .isNumeric()
    .withMessage('You have to fill both Periodic Limit and Periodic Limit length'),
   
    check('monthly_limit')
    .if((value,{req})=>value)
    .isNumeric()
    .withMessage('please fill monthly_limit with valid value'),

    check('max_limit')
    .if((value,{req})=>value)
    .isNumeric()
    .withMessage('please fill max_limit with valid value'),

    check('global_daily_limit')
    .if((value,{req})=>value)
    .isNumeric()
    .withMessage('please fill global_daily_limit with valid value'),

    check('partner_id')
    .if((value,{req})=>value && req.body.voucher_type ==='point')
    .isEmpty()
    .withMessage('point vouchers cant have partnet_id'),
    check('partner_id')
    .if((value,{req})=>value && req.body.voucher_type ==='system')
    .isString()
    .withMessage('please fill partner_id with valid value'),


    validator

]
const addAVoucherRedemValidator = [

    check('voucher_id').not().isEmpty().isUUID()
    .withMessage('please fill voucher_id with valid value'),
    
    check('point_account_id')
    .not().isEmpty().isUUID()
    .withMessage('please fill point_account_id with valid value'),

    check('voucher_redemption_id')
    .if((value,{req})=>value)
    .not().isEmpty().isUUID()
    .withMessage('please fill voucher_redemption_id with valid value'),
       
    
    check('amount')
    // .if((value,{req})=>value || req.body.voucher_redemption_id)
    .not().isEmpty().isNumeric()
    .withMessage('please fill amount with valid value'),

    check('date')
    .if((value,{req})=>req.body.test_mode)
    .not().isEmpty()
    .withMessage('please fill date with valid value'),

    check('date')
    .if((value,{req})=>!req.body.test_mode)
    .isEmpty()
    .withMessage('you cant add date to transaction'),

    validator

]


module.exports = { addAVoucherValidator ,addAVoucherRedemValidator};
