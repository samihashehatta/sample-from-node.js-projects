
const { validator } = require('../../middlewares/validateRequest');
const { check } = require('express-validator');
const moment = require('moment')
const checkOnValidation = [

    check('voucher.id')
    .if((value,{req})=>req.body.voucher)
    .not().isEmpty().isUUID()
    .withMessage('please fill voucher.id with valid value'),
       

    check('voucher.spendingRuleId')
    .if((value,{req})=>req.body.voucher)
    .not().isEmpty().isUUID()
    .withMessage('please fill voucher spendingRuleId with valid value'),
        

    check('spending_campaign.id')
    .if((value,{req})=>req.body.spending_campaign)
    .not().isEmpty().isUUID()
    .withMessage('please fill spending_campaign.id with valid value'),
       

    check('spending_campaign.tierSpendingRuleId')
    .if((value,{req})=>req.body.spending_campaign)
    .not().isEmpty().isUUID()
    .withMessage('please fill spending_campaign tierSpendingRuleId with valid value'),
        

    check('amount')
    .if((value,{req})=>value)
    .not().isEmpty().isNumeric().custom((value,{req})=>{
        if(Math.sign(value) == -1) return false
        else return true
    })
    .withMessage('amount must be positive value '),

    check('company_id')
    .not().isEmpty()
    .withMessage('please fill company_id with a value '),
    
    
    check('user_id')
    .not().isEmpty()
    .withMessage('please fill user_id with a value '),
    
    check('date')
    .if((value,{req})=>req.body.test_mode)
    .not().isEmpty() 
    .withMessage('please fill date with a value in test mode'),
    check('date')
    .if((value,{req})=>!req.body.test_mode)
    .isEmpty()
    .withMessage('you can only pass value in test mode '),
    
    
    validator

]

module.exports = {checkOnValidation};
