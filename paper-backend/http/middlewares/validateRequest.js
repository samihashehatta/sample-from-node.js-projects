const { validationResult} = require('express-validator');
const {generalResponse} = require('../requests/helpers/responseBody');
const validator =  (req, res, next) => {
    const errors = validationResult(req);
    const msg = errors.errors.map((err)=>err.msg)
    return (!errors.isEmpty())?res.status(400).jsonp(generalResponse({},errors.array(),"",`${msg}`)):next();
}

module.exports = {validator};