
const jwt = require('jsonwebtoken');
const userRepo = require('../../system/repositories/userRepo')
const {generalResponse} = require('../requests/helpers/responseBody')
const authenticate = (req,res,next) =>{
    let token = req.header('Authorization');
    if(!token) return res.status(401).send(generalResponse({},[],'you are unauthorized user'))
    token = token.replace('Bearer', '').trim()
    const decoded  = jwt.verify(token, process.env.JWT_SECERT)

    userRepo.findById(decoded.id)
    .then(user =>{
        if(!user)res.status(401).send(generalResponse({},[],'you are unauthorized user'))
        else{
            if(user.status === 'suspended')res.status(401).send(generalResponse({},[],'you are unauthorized user'))
            else{
            req.user = user
            req.token = token
            next()
            }
        }
    })
    .catch(err => res.status(401).send(generalResponse({},[],err.message,'you are unauthorized user')))

}

module.exports = {authenticate}