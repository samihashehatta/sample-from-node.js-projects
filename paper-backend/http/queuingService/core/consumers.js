const loyaltyDigPayInSuccessConsumer=  require('./loyaltyDigPayInSuccessConsumer')
const loyaltyInvoiceSentConsumer = require('./loyaltyInvoiceSentConsumer')
const loyaltyCompanyVerificationConsumer = require('./loyaltyCompanyVerificationConsumer')
const loyaltySubscriptionSuccessConsumer = require('./loyaltySubscriptionSuccessConsumer') 

const listentToAll = () => {
    // loyaltyDigPayInSuccessConsumer.listen()
    loyaltyCompanyVerificationConsumer.listen()
    loyaltyInvoiceSentConsumer.listen()
    loyaltySubscriptionSuccessConsumer.listen()
}

module.exports ={
    listentToAll
}