const channel = require('./channel');

const logger = require('../../sys/logger')

const queue = "loyaltyDigPayInSuccess";

const {forQueues} = require('../../business/earningCampaignSerivce')
const listen = () => {

    channel.then((ch) => {
         ch.consume(queue,(msg) => {
            const body = JSON.parse(msg.content.toString())
                         
                       forQueues(queue,body)
                       .then(response =>{
                           logger.log('general','info',`earning done successfuly ${response}`,'loyaltyDigPayInSuccess queue funcation')
                           ch.ack(msg)
                           return response
                       })
                       .catch(err=>{
                        logger.log('general','error',`error in earning from ${queue} due to :${err}`,'loyaltyDigPayInSuccess queue funcation')
                        ch.ack(msg)

                        return err
                       })

                     }
                    ,
                  {
                      noAck: false
                  });
    return ch
    }).catch((error) => logger.log(
        'general',
        'error',
        `We can't open a channel because we had some issue ${error}`,
        'rabbit chanel'));  
}

module.exports = {listen};
