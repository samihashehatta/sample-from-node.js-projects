const channel = require('./channel');

const logger = require('../../sys/logger')

const queue = "loyaltySubscriptionSuccess";
const {forQueues} = require('../../business/earningCampaignSerivce')

const parseToJsonChecker = (msg) =>{
    let body 
    try{
         body = JSON.parse(msg) 
        return body
    }
    catch(e){
        body = null
        return body
    }
}
const listen = () => {

    channel.then((ch) => {
         ch.consume(queue,(msg) => {
             console.log('mssege' , msg.content.toString())
        
             const body = parseToJsonChecker(msg.content.toString())
             console.log('body' , body)

                    if(body){
                        forQueues(queue,body)
                        .then(response =>{
                            logger.log(
                                'general',
                                'info',
                                `message with company id :${body.company_id} and doc_id ${body.doc_id}`,
                                'for queues function')
                            ch.ack(msg)
                            return response
                        })
                        .catch(err=>{
                            logger.log(
                                'general',
                                'error',
                                `message company id :${body.company_id} and doc_id ${body.doc_id} and the error is ${err}`,
                                'for queues function')
                            ch.ack(msg)
                            console.log('error',err)

                            return err
                        })
                        }
                    else ch.ack(msg)

                }
                    ,
                  {
                      noAck: false
                  })
    return ch
    }).catch((error) => logger.log(
        'general',
        'error',
        `We can't open a channel because we had some issue ${error}`,
        'rabbit chanel'));  
}
module.exports = {listen};
