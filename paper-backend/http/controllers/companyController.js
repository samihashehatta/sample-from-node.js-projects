const companyRepo = require('../../system/repositories/companyRepo')

const {generalResponse} =  require('../requests/helpers/responseBody');

const companySubscriptionRepo  = require('../../system/repositories/companySubscriptionRepo')

const companyTierRepo  = require('../../system/repositories/companyTierRepo')
const companyService  = require('../../system/services/business/companyService')


const getAllCompanies = (req,res) =>{

    companyService.getPaginOrNot(req).then((response)=>{
            if(req.query.range) {
                res.set( 'Access-Control-Expose-Headers', 'Content-Range')
                res.set('Content-Range',`point-transactions 0-4/${response.total}`)
                response= response.docs
            }
            res.send(generalResponse(response,[],'','done'))
        })
        .catch(error=>{
            res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
        })
        
    }


const getCompany = (req,res) =>{
    const id  = req.params.id
    companyService.getACompny(id)
    .then(company =>{
        res.send(generalResponse(company))
    }).catch(err=>{
        res.status(400).send(generalResponse({},[],`${err}`,`${err}`))

    })
}

const findAllCompaniesSubs = (req,res) =>{

    companyService.getPaginOrNotSub(req).then((response)=>{
        if(req.query.range) {
            res.set( 'Access-Control-Expose-Headers', 'Content-Range')
            res.set('Content-Range',`point-transactions 0-4/${response.total}`)
            response= response.docs
        }
        res.send(generalResponse(response,[],'','done'))
    })
    .catch(error=>{
        res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
    })
    
}
const findCompanyLoyaltyTier = (req,res) =>{
    let range = req.query.range? JSON.parse(req.query.range):[]
    let filter = req.query.filter? JSON.parse(req.query.filter):{}
    let limit = filter.companyId?1:range[1]
    companyTierRepo.getPaginatedTier(range[0],limit,{...filter})
    .then(response=>{
        res.set( 'Access-Control-Expose-Headers', 'Content-Range')
        res.set('Content-Range',`point-transactions 0-4/${response.total}`)
        res.send(generalResponse(response.docs,[],'','done'))
    })
    .catch(error=>{
        res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
    })
}


module.exports = {
    getAllCompanies,
    getCompany,
    findAllCompaniesSubs,
    findCompanyLoyaltyTier
}