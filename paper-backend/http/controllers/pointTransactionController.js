const _ = require('lodash');
const express = require('express');
const pointTransRepo = require('../../system/repositories/pointTransactionRepo')
const pointAccountRepo = require('../../system/repositories/pointAccountRepo')
const pointTransTypeRepo = require('../../system/repositories/pointTransactionTypesRepo')
const pointconsolidationRepo = require('../../system/repositories/pointconsolidationRepo')
const pointTransactionService = require('../../system/services/business/pointTransactionService')
const cronJob = require('../../system/services/sys/cronJob')
const {generalResponse} =  require('../requests/helpers/responseBody');


const addNewPointTrans = (req,res) =>{
    const body = _.pick(req.body,[
                'date',
                "voucher_id",
                'amount',
                'spending_campaign_id',
                'point_account_id',
                'type',
                'reference',
                'document_id',
                "document_type",
                "partner_id",
                'partner_type',
                'document_redem_id',
                'document_redem_type'])

    pointTransactionService.filterByTransType(body,body.type)

    .then(types=>{

        res.send(generalResponse({pointTransaction:types}))
    })
    .catch(error=>{
       
        res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
    })

}


const cancelPointTrans = (req,res) =>{
    
    const id = req.params.id
    pointTransactionService.cancelTrans(id)
    .then(pointTrans =>{

        res.send(generalResponse({pointTransaction:pointTrans}))
    })
    .catch(error=>{
        
        res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
    })
}


const finishPointTrans = (req,res) =>{
    
    const id = req.params.id;
    pointTransactionService.successfulTrans(id)
    .then(pointTrans =>{

        res.send(generalResponse({pointTransaction:pointTrans}))
    })
    .catch(error=>{
        res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
    })
}

const getPointTrans = (req,res) =>{
    
    pointTransRepo.findPointTrans({id:req.params.id})
    .then(pointTrans =>{

        res.send(generalResponse({pointTransaction:pointTrans}))

    }).catch(error=>{
        
        res.status(400).send(generalResponse({},[],`${error}`))
    })
}

const getAccountPointTrans = (req,res) =>{

    const id = req.params.id
    const page = parseInt(req.query.page)?parseInt(req.query.page):1;

    pointTransRepo.getPointAccountTransactions(page,3,id)
    .then(account =>{

        res.send(generalResponse({pointTransactions:[...account.docs],'limit':3,'pages':account.pages,'total':account.total}))

    }).catch(error=>{
        res.status(400).send(generalResponse({},[],`${error}`))
    })
}

const getAllPointTransTypes = (req,res) =>{
    pointTransTypeRepo.findAllPointTransTypes()
    .then(types=>{
        res.send(generalResponse(types))
    })
    .catch(error=>{
        res.status(400).send(generalResponse({},[],`${error}`,'cant add new type'))
    })

}

const addPointTransType = (req,res) =>{
    const body = _.pick(req.body,['code','name'])
    pointTransTypeRepo.createNewPointTransType(body)
    .then(type=>{
        res.send(generalResponse({type}))
    })
    .catch(error=>{
        res.status(400).send(generalResponse({},[],`${error}`,'cant add new type'))
    })

}
const pointConsiladation = (req,res) =>{
    const id = req.params.id
    cronJob.runPointConsildation(id)
    res.send(generalResponse({},'','','your consildation is being created'))
}
const getPointConsiladation = (req,res) =>{
    const id = req.params.id
    pointconsolidationRepo.findOne({id:id})
    .then(response=>{
        res.send(generalResponse({pointConsiladation:response}))
    })
    .catch(error=>{
        res.status(400).send(generalResponse({},[],`${error}`,'cant consiladte points'))
    })
}
const getPointConsiladations = (req,res) =>{
    let range = req.query.range? JSON.parse(req.query.range):[]
    let filter = req.query.filter? JSON.parse(req.query.filter):{}
    pointconsolidationRepo.getAllTransWithPaginite(range[0],range[1],{...filter})
    .then(response=>{
        res.set( 'Access-Control-Expose-Headers', 'Content-Range')
        res.set('Content-Range',`point-transactions 0-4/${response.total}`)
        res.send(generalResponse(response.docs,[],'','done'))
    })
    .catch(error=>{
        res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
    })
}
const getPointAccountConsiladations = (req,res) =>{
    let id =req.params.id
    pointconsolidationRepo.findAll({pointAccountId:id})
    .then(response=>{
        res.send(generalResponse(response,[],'','done'))
    })
    .catch(error=>{

        res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
    })
}
const createPointTrans = (req,res) => {
    const body = _.pick(req.body,[
        'amount',
        'value',
        'point_account_id',
        'type',
        'reference',
        'document_id',
        "document_type",
])
    pointTransactionService.adjOrBonus(body)
    .then(response=>{
        res.send(generalResponse(response,[],'','done'))
    })
    .catch(error=>{

        res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
    })
    
}
const getAllTrans = (req,res) => {
    let range = req.query.range? JSON.parse(req.query.range):[]
    let filter = req.query.filter? JSON.parse(req.query.filter):{}
    pointTransRepo.getAllTransWithPaginite(range[0],range[1],{...filter})
    .then(response=>{
        res.set( 'Access-Control-Expose-Headers', 'Content-Range')
        res.set('Content-Range',`point-transactions 0-4/${response.total}`)
        res.send(generalResponse(response.docs,[],'','done'))
    })
    .catch(error=>{
        res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
    })
}
module.exports ={
    getAllTrans,
    addNewPointTrans,
    cancelPointTrans,
    finishPointTrans, 
    getPointTrans,
    getAccountPointTrans,
    getAllPointTransTypes,
    addPointTransType,
    pointConsiladation,
    getPointConsiladation,
    getPointConsiladations,
    createPointTrans,
    getPointAccountConsiladations
}