const _ = require('lodash');
const express = require('express');
const promotionServices = require('../../system/services/business/promotionService')
const {generalResponse} =  require('../requests/helpers/responseBody');
const companyTierRepo = require('../../system/repositories/companyTierRepo')
const TierRepo = require('../../system/repositories/loyalityTierSpendingRuleRepo')

exports.getAllPromotions =(req,res) =>{
    let body = _.pick(req.body,['user_id','spending_rule_id','currency','amount','company_id'])
     body ={
       userId : body.user_id,
       spendingRuleId :body.spending_rule_id,
       currency:body.currency,
       amount:body.amount,
       companyId:body.company_id
        }
        
    promotionServices.findAllPromotions(body)
    .then(promotions =>{
      return res.send(generalResponse({"promotions":promotions},[],'','promotions found'))
    })
    .catch(error =>{
        return res.status(400).send(generalResponse({},[],`${error}`,"can't find promotions"))
    })
}

exports.validatePromotion = (req,res) =>{
  const body = _.pick(req.body,['voucher','spending_campaign','user_id','amount','company_id','date'])


  promotionServices.validatePromtion(body)

  .then(response =>{

    return res.send(generalResponse({"promotion":response},[],'','promotions validated'))

  })
  .catch(err =>{
    return res.status(400).send(generalResponse({},[],`${err}`,`${err}`))

  })

    
}
