const _ = require('lodash');

const tierSpendingRuleRepo = require('../../system/repositories/loyalityTierSpendingRuleRepo')

const tierSpendingRuleService = require('../../system/services/business/tierSpendingRuleService')

const {generalResponse} =  require('../requests/helpers/responseBody');


const getAllTierSpendingRule = (req,res) =>{
    tierSpendingRuleService.getPaginOrNot(req).then((response)=>{
        if(req.query.range) {
            res.set( 'Access-Control-Expose-Headers', 'Content-Range')
            res.set('Content-Range',`point-transactions 0-4/${response.total}`)
            response= response.docs
        }
        res.send(generalResponse(response,[],'','done'))
    })
    .catch(error=>{
        res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
    })
}

const addNewTierSpendingRule = (req,res) =>{

    const body = _.pick(req.body,[
            "loyalty_tier_id",
            "spending_rule_id",
            "currency",
            "fixed_point_discount",
            "ratio_point_value",
            "min_transaction_value",
            "periodic_limit",
            "periodic_limit_length",
            "monthly_limit",
            "max_limit" ])
    
        tierSpendingRuleService.addTierSpendingRule(body)
        .then(tiersRule=>{
            res.send(generalResponse(tiersRule))
        })
        .catch(err=>{

            res.status(400).send(generalResponse({},[],`${err}`,`${err}`))
        })
    

}

const getTierSpendingRule = (req,res) =>{
    const tierSpendingRuleId = req.params.id

    tierSpendingRuleRepo.findOneLoyaltyTierSpendingRule({id:tierSpendingRuleId})
    .then(tierSpendingRule =>{
        if (!tierSpendingRule )
        return res.status(400).send(generalResponse({},[],'','No Tier Spending Rule Found'))
     
        else return res.send(generalResponse(tierSpendingRule))
    })

    .catch(err=>{
        res.status(400).send(generalResponse({},[],`${err}`,`${err}`))
    })
}
const deleteTierSpeningRule = (req,res) =>{
    const id = req.params.id

    tierSpendingRuleService.deleteTierSpendingRule(id)
    .then(campaign =>{
        return res.status(200).send(generalResponse({"tierSpendingRule":campaign},[],'','tier rule deleted'))
      })
      .catch(error =>{
          return res.status(400).send(generalResponse({},[],`${error}`,"can't delete tier rule"))
      })
    
}


module.exports= {
    getAllTierSpendingRule,
    addNewTierSpendingRule,
    getTierSpendingRule,
    deleteTierSpeningRule
}