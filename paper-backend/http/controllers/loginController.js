const _ = require('lodash');
const express = require('express');
const userRepo = require('../../system/repositories/userRepo')
const {generalResponse} =  require('../requests/helpers/responseBody');
const queueTest = require('../../system/services/business/earningCampaignSerivce')


exports.login =(req,res) =>{
    let body = _.pick(req.body,['email','password'])
    userRepo.findByEmail(body)
    .then(foundUser =>{
        if(!foundUser) res.status(401).send(generalResponse({},[],'unauthorized'))
        else{
            foundUser.comparePassword(body.password)
            .then((isMatch)=>{
                if (isMatch) {
                    let token = foundUser.generateToken()
                    foundUser ={
                       id:foundUser.id,
                       name:foundUser.name ,
                       email:foundUser.email,
                       status:foundUser.status ,
                       type:foundUser.type,
                       createdAt:foundUser.createdAt,
                       updatedAt:foundUser.updatedAt
                    }
                    return res.header('Authorization',token).send(generalResponse({"user":foundUser, "token": token }));
                }
                else res.status(401).send(generalResponse({},[],'password is wrong'))
            })
            .catch(err =>res.status(401).send(generalResponse({},[],'unauthorized')))
        }
    })
    .catch(err =>{
        res.status(401).send(generalResponse({},[],'unauthorized'))
    })
}

exports.signup = (req,res) =>{
    let body = _.pick(req.body,['name','email','password','status','type'])
    userRepo.createUser(body)
    .then(user =>{
        user ={
            id:user.id,
            name:user.name ,
            email:user.email,
            status:user.status ,
            type:user.type,
            createdAt:user.createdAt,
            updatedAt:user.updatedAt
         }
        res.send(generalResponse({user}))
    })
    .catch(err=>{
        console.log(err)
        res.status(400).send(generalResponse({},[],`${err}`,`${err}`))
    })
}
exports.getAllUsers = (req,res)=>{
    let range = req.query.range? JSON.parse(req.query.range):[]
    let filter = req.query.filter? JSON.parse(req.query.filter):{}
    userRepo.getPaginite(range[0],range[1],{...filter})
    .then(response=>{
        res.set( 'Access-Control-Expose-Headers', 'Content-Range')
        res.set('Content-Range',`point-transactions 0-4/${response.total}`)
        res.send(generalResponse(response.docs,[],'','done'))
    })
    .catch(error=>{
        res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
    })
}
exports.test = (req,res) =>{
    console.log('testtttttt2',req.body)
      queueTest.forQueues(req.body.queueName,req.body.body)
    .then(response =>{
       res.send(response)
     })
     .catch(err=>{
      res.send(err)
      console.log(err)
    })
}