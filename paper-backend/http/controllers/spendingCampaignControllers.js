const _ = require('lodash');
const express = require('express');
const spendingCampaignRepo = require('../../system/repositories/spendingCampaignRepo')
const spendingCampaignService = require('../../system/services/business/spendingCampaignService')
const {generalResponse} =  require('../requests/helpers/responseBody');


exports.getSpendingCampaign = (req,res) =>{
    const spendingCampaignId = req.params.spending_campaign_id

    spendingCampaignRepo.findSpendingCampaign({id:spendingCampaignId})
    .then(campaign =>{
        if(!campaign) return res.status(400).send(generalResponse({},[],'',"no campaigns found "))
        else return res.send(generalResponse(campaign,[],'','campaigns found'))
      })
      .catch(error =>{
          return res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
      })
}
exports.getAllSpendingCampaign = (req,res) =>{

    spendingCampaignService.getPaginOrNot(req).then((response)=>{
        if(req.query.range) {
            res.set( 'Access-Control-Expose-Headers', 'Content-Range')
            res.set('Content-Range',`point-transactions 0-4/${response.total}`)
            response= response.docs
        }
        res.send(generalResponse(response,[],'','done'))
    })
    .catch(error=>{
        res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
    })
    
}
exports.addNewSpendingCampaign = (req,res) =>{

    const body = _.pick(req.body,[
        'tier_spending_rule_id',
        'name',
        'start_date',
        'end_date',
        'banner_url',
        'terms',
        'fixed_point_spent',
        'global_daily_limit'])

    const campaign = {
            tierSpendingRuleId:body.tier_spending_rule_id,
            name:body.name,
            startDate:body.start_date,
            endDate:body.end_date,
            bannerUrl:body.banner_url,
            terms:body.terms,
            fixedPointSpent:body.fixed_point_spent,
            globalDailyLimit:body.global_daily_limit

        }

    spendingCampaignService.addSpedningCamp(campaign)
    .then(campaign =>{
        return res.status(201).send(generalResponse(campaign,[],'','campaign created'))
      })
      .catch(error =>{
          return res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
      })
    


}

exports.deleteSpendingCampaign = (req,res) =>{

    const spendingCampaignId = req.params.spending_campaign_id

    spendingCampaignService.deleteACampaign(spendingCampaignId)
    .then(campaign =>{
        return res.status(200).send(generalResponse(campaign,[],'','campaign created'))
      })
      .catch(error =>{
          return res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
      })
    


}