const _ = require('lodash');

const earningRulesRepo = require('../../system/repositories/earningRulesRepo')

const {generalResponse} =  require('../requests/helpers/responseBody');


const getOneOrMoreEarningRules = async (req) =>{
    if(req.params.id) return await earningRulesRepo.findOne({id:req.params.id})
    else return await earningRulesRepo.getAll({})
}

const getAllEarningRules = (req,res) =>{

    getOneOrMoreEarningRules(req)

    .then(response => {

        if(Array.isArray(response)) return res.send(generalResponse(response))

        else return res.send(generalResponse(response))
    })
    .catch(error=> res.status(400).send( generalResponse({},[],`${error}`,`${error}`) ) )
}

module.exports= {
     getAllEarningRules ,
  
}