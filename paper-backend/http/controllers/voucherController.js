const _ = require('lodash');

const voucherRepo = require('../../system/repositories/voucherRepo')

const voucherRedemptionRepo = require('../../system/repositories/voucherRedemptionRepo')

const voucherService = require('../../system/services/business/voucherService')

const {generalResponse} =  require('../requests/helpers/responseBody');

const getAllVouchers = (req,res) =>{

    voucherService.getPaginOrNot(req).then((response)=>{
        if(req.query.range) {
            res.set( 'Access-Control-Expose-Headers', 'Content-Range')
            res.set('Content-Range',`point-transactions 0-4/${response.total}`)
            response= response.docs
        }
        res.send(generalResponse(response,[],'','done'))
    })
    .catch(error=>{
        res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
    })
}

const addVoucher = (req,res) => {
    const body = _.pick(req.body,[
        'voucher_type',
        'redemption_point_amount',
        'expiry_length',
        "spending_rule_id",
        "start_date",
        "expiry_date",
        "currency",
        "banner_url",
        "terms",
        "max_discount_amount",
        "fixed_voucher_amount",
        "pct_voucher_discount",
        "min_transaction_value",
        "periodic_limit",
        "periodic_limit_length",
        "monthly_limit",
        "global_daily_limit",
        "name",
        'partner_id',
        "max_limit" ])

    const voucher = {
        voucherType:body.voucher_type,
        redemptionPointAmount:body.redemption_point_amount,
        expiryLength:body.expiry_length,
        spendingRuleId:body.spending_rule_id,
        startDate:body.start_date,
        expiryDate:body.expiry_date,
        currency:body.currency,
        bannerUrl:body.banner_url,
        terms:body.terms,
        partnerId:body.partner_id,
        maxDiscountAmount:body.max_discount_amount,
        name:body.name,
        fixedVoucherAmount:body.fixed_voucher_amount,
        pctVoucherDicount:body.pct_voucher_discount,
        minTransactionValue:body.min_transaction_value,
        periodicLimit:body.periodic_limit,
        periodicLimitLength:body.periodic_limit_length,
        monthlyLimit:body.monthly_limit,
        globalDailyLimit:body.global_daily_limit,
        maxLimit:body.max_limit
    }

    voucherRepo.createOne(voucher)
    .then(voucher=>{
        res.send(generalResponse(voucher,[],'','created'))
    })
    .catch(error =>{
        res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
    })

}

const getVoucher =  (req,res) =>{

    const query = {id:req.params.id}
    
    voucherRepo.findOne(query)

    .then(voucher =>{
        res.send(generalResponse(voucher,[],'','created'))
    })
    .catch(error =>{
        res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
    })

}

const deleteVoucher = (req,res) =>{

    const voucherId = req.params.id

    voucherService.deleteAVoucher(voucherId)
    .then(campaign =>{
        return res.status(200).send(generalResponse(campaign,[],'','campaign deleted'))
      })
      .catch(error =>{
          return res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
      })
    


}

const redemVoucher = (req,res) =>{
    const body = _.pick(req.body,[
        'voucher_id',
        'point_account_id',
        'voucher_redemption_id',
        'voucher_id',
        'partner_id',
        'partner_type',
        'document_trans_id',
        'document_trans_type',
        'document_id',
        'document_type',
        'amount',
        'date',
        'reference'
    ])
    voucherService.createPendingRedem(body)
    .then(response =>{
        res.send(generalResponse({voucherRedemption:response}))

    })
    .catch(err=>{
        
        res.status(400).send(generalResponse({},[],`${err}`,`${err}`))

    })
}

const cancelVoucherRedem =(req,res)=>{

    const id = req.params.id

    voucherService.cancelRedem(id)
    .then(response =>{
        res.send(generalResponse({voucherRedemption:response}))

    })
    .catch(err=>{
        res.status(400).send(generalResponse({},[],`${err}`,`${err}`))

    })
}

const finishVoucherRedem = (req,res) =>{

    const id = req.params.id
  
    voucherService.finishRedem(id)
    .then(response =>{
        res.send(generalResponse({voucherRedemption:response}))

    })
    .catch(err=>{

        res.status(400).send(generalResponse({},[],`${err}`,`${err}`))

    })
}

const getARedem = (req,res) =>{

    const id = req.params.id
  
    voucherRedemptionRepo.findOneWithPointAndVoucher({id})
    .then(response =>{
        res.send(generalResponse({voucherRedemption:response}))

    })
    .catch(err=>{

        res.status(400).send(generalResponse({},[],`${err}`,'error in redemptions'))

    })
}
const getRedems = (req,res) =>{
    let range = req.query.range? JSON.parse(req.query.range):[]
    let filter = req.query.filter? JSON.parse(req.query.filter):{}
    voucherRedemptionRepo.getPaginatedRedeem(range[0],range[1],{...filter})
    .then(response=>{
        res.set( 'Access-Control-Expose-Headers', 'Content-Range')
        res.set('Content-Range',`point-transactions 0-4/${response.total}`)
        res.send(generalResponse(response.docs,[],'','done'))
    })
    .catch(error=>{
        res.status(400).send(generalResponse({},[],`${error}`,`${error}`))
    })
}

const getAccountRedem = (req,res) =>{

    const id = req.params.id
    const page = parseInt(req.query.page)?parseInt(req.query.page):1;
  
    voucherRedemptionRepo.getPointAccountRedeem(page,3,id)
    .then(response =>{
        res.send(generalResponse({voucherRedemption:[...response.docs],'limit':3,'pages':response.pages,'total':response.total}))


    })
    .catch(err=>{

        res.status(400).send(generalResponse({},[],`${err}`,'error in redemptions'))

    })
   
}
module.exports ={
    getAllVouchers,
    addVoucher,
    getVoucher,
    deleteVoucher,
    redemVoucher,
    cancelVoucherRedem,
    getARedem,
    finishVoucherRedem,
    getAccountRedem,
    getRedems
}