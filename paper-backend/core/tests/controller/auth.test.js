process.env.NODE_ENV= 'test';
const chai= require('chai');
const chaiHttp=require('chai-http');
const app = require('../../server');
// const assert = chai.assert;
chai.use(chaiHttp);
chai.should()
describe('testing authenticated user ',()=>{
let token;
    before((done)=>{
            
            chai.request(app)
            .post('/api/v1/user/login')
            .set('Content-type','application/json')
            .send({
            
                "email":"samiha@gmail.com",
                "password":"sa1223"
            
            })
            .end((err,res)=>{
                token=res.body.data.token
                done()
        });

        });
        it('should return authenticated user',(done)=>{
            
            chai.request(app)
            .get('/api/v1/user/test-auth')
            .set('Content-type','application/json')
            .set('authorization',token)
            .end((err,res)=>{

                res.should.have.status(200);
                res.type.should.equal('application/json');
                res.body.should.be.a('object');
                res.error.should.equal(false);
                res.body.data.user.should.include.keys(
                        'id',
                        'name',
                        'email',
                        'password',
                        'status',
                        'createdAt',
                        'updatedAt',
                 )
                res.body.data.user.status.should.equal('active')
                done()
            });
        });
});

describe('testing not authenticated user ',()=>{
    let token;
        before((done)=>{
                
                chai.request(app)
                .post('/api/v1/user/login')
                .set('Content-type','application/json')
                .send({
                
                    "email":"somone@gmail.com",
                    "password":"sa1223"
                
                })
                .end((err,res)=>{
                    token=null
                    done()
            });
    
            });
            it('should return authenticated user',(done)=>{
                
                chai.request(app)
                .get('/user/test-auth')
                .set('Content-type','application/json')
                .set('authorization',token)
                .end((err,res)=>{
    
                    res.should.have.status(500);
                   
                    done()
                });
            });
    });
    
    
describe('testing suspended user ',()=>{
        let token;
            before((done)=>{
                    
                    chai.request(app)
                    .post('/api/v1/user/login')
                    .set('Content-type','application/json')
                    .send({
                    
                        "email":"abdalla@gmail.com",
                        "password":"ab1223"
                    
                    })
                    .end((err,res)=>{
                        token=res.body.data.token
                        done()
                });
        
                });
                it('should not pass suspended user',(done)=>{
                    
                    chai.request(app)
                    .get('/user/test-auth')
                    .set('Content-type','application/json')
                    .set('authorization',token)
                    .end((err,res)=>{
        
                        res.should.have.status(401);
                       
                        done()
                    });
                });
    });