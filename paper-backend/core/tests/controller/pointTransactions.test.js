process.env.NODE_ENV= 'test';
const chai= require('chai');
const chaiHttp=require('chai-http');
const app = require('../../server');
const companyTierRepo =require('../../system/repositories/companyTierRepo')
const loyaltyTierRepo =require('../../system/repositories/loyaltyTierRepo')
const spendingRuleRepo =require('../../system/repositories/spendingRuleRepo')
const loyalityTierSpendingRuleRepo =require('../../system/repositories/loyalityTierSpendingRuleRepo')
const spendingCampaignRepo =require('../../system/repositories/spendingCampaignRepo')
const pointAccountRepo =require('../../system/repositories/pointAccountRepo')

chai.use(require('chai-string'));
// const assert = chai.assert;
chai.use(chaiHttp);
chai.should()

var pointAccountId,account2,
balance,spedningCampaignId,token,pointTransId,pointTransId2,tierSpendingRuleId,spedningCampaignId;

const testData = async() =>{

    let body = {code:"transTest",name:"TRANS TEST",point_threshold:5000}
    const createLoyaltyTier = await loyaltyTierRepo.create(body)
    const LoyaltyTier = createLoyaltyTier

    body = {
        name:'camp',
        companyId:"compny1",
        loyalityTierId:`${LoyaltyTier.id}`,
        startDate:new Date('2019-12-02'),
        endDate:new Date('2020-01-01')
    }
    const companyTier = await companyTierRepo.createCompanyLoyaltyTier(body)

    body ={ 
            code:"TRANS",
            name:"TRANS TEST",
            event_type:"oneTime",
            event_status:"active"
        }
    const spendingRule = await spendingRuleRepo.create(body)


    body= {
        loyalityTierId:LoyaltyTier.id,
        spendingRuleId:spendingRule.id,
        currency:"IDR",
        fixedPointDiscount:50000,
        minTransactionValue:10000,
        periodicLimit:5,
        periodicLimitLength:7
    }
    const loyalityTierSpendingRule  = await loyalityTierSpendingRuleRepo.createLoyaltyTierSpendingRule(body)

    tierSpendingRuleId = loyalityTierSpendingRule.id

    const obj = {
        tierSpendingRuleId:loyalityTierSpendingRule.id,
        name:"spending camp",
        fixedPointSpent:15,
        globalDailyLimit:1,
        startDate:"2019-12-03T14:57:19.117Z",
        endDate:"2020-02-01T14:57:19.117Z"}
    const spendingCampaigns = await spendingCampaignRepo.createSpendingCampaign(obj)
        spedningCampaignId=spendingCampaigns.id
    let account = {
        
            companyId:"compny1",
            userId:"testUser1",
            isOwner:true,
            verified:true
    }
    const pointAccount =  await pointAccountRepo.createNewPointAccount(account)
     let paccount = {
        
        companyId:"compny1",
        userId:"testUser2",
        isOwner:false,
        verified:true
}
    const pointAccount2 =  await pointAccountRepo.createNewPointAccount(paccount)
    account2 = pointAccount2.id
    pointAccountId = pointAccount.id
    balance =pointAccount.balance
    return pointAccount

};


describe('testing point transactions ',()=>{
    before((done)=>{
            
        chai.request(app)
        .post('/api/v1/user/login')
        .set('Content-type','application/json')
        .send({
        
            "email":"samiha@gmail.com",
            "password":"sa1223"
        
        })
        .end((err,res)=>{
            token=res.body.data.token
            done()
    });

    });

    describe('creating testing data ',()=>{

    it('should make test data',(done)=>{
        testData()
        .then(companyTier=>{
            done()
        })
    });
    });

    describe('testing validiton and creation of point transaction',()=>{

        it('should make a point transaction',(done)=>{
            chai.request(app)
            .post('/api/v1/point-transactions/pending')
            .set('Content-type','application/json')
            .set('authorization',token)
            .send({
                    "spending_campaign_id":`${spedningCampaignId}`,
                    "point_account_id": `${pointAccountId}`,
                    "type":"SPEN",
                    "amount":15000,
                    "reference":"smsmmsms"
            })
            .end((err,res)=>{
            
                res.should.have.status(200);
                res.type.should.equal('application/json');
                res.body.data.pointTransaction.should.be.a('object');
                res.body.data.pointTransaction.should.include.keys(
                    'id',
                    'pointAccountId',
                    'spendingCampaignId',
                    'reference',
                    'creditAmount',
                    'status',
                    'pointTransactionTypeId'
                );
                res.body.data.pointTransaction.status.should.equal('pending')
                pointTransId = res.body.data.pointTransaction.id
                done()
        });
        });
        it('should return validation error',(done)=>{
            chai.request(app)
            .post('/api/v1/point-transactions/pending')
            .set('Content-type','application/json')
            .set('authorization',token)
            .send({
                    "spending_campaign_id":`${spedningCampaignId}`,
                    "point_account_id": `${pointAccountId}`,
                    "type":"SPEN",
                    "amount":10,
                    "reference":"smsmmsms"
            })
            .end((err,res)=>{
                res.should.have.status(400);
                
                res.body.debugError.should.equal("You haven't reached the minimum transaction amount 10000")


                done()
        });
        });
        it('should make a second point transaction',(done)=>{
            chai.request(app)
            .post('/api/v1/point-transactions/pending')
            .set('Content-type','application/json')
            .set('authorization',token)
            .send({
                    "spending_campaign_id":`${spedningCampaignId}`,
                    "point_account_id": `${pointAccountId}`,
                    "type":"SPEN",
                    "amount":15000,
                    "reference":"smsmmsms"
            })
            .end((err,res)=>{
            
                res.should.have.status(200);
                res.type.should.equal('application/json');
                res.body.data.pointTransaction.should.be.a('object');
                res.body.data.pointTransaction.should.include.keys(
                    'id',
                    'pointAccountId',
                    'spendingCampaignId',
                    'reference',
                    'creditAmount',
                    'status',
                    'pointTransactionTypeId'
                );
                res.body.data.pointTransaction.status.should.equal('pending')
                pointTransId2 =  res.body.data.pointTransaction.id

                done()
        });
        });

        it('should return spending campaign ',(done)=>{
            chai.request(app)
            .get('/api/v1/promotions/validate')
            .set('Content-type','application/json')
            .set('authorization',token)
            .send({
                "spending_campaign":{
                    
                        "id": `${spedningCampaignId}`,
                        "tierSpendingRuleId": `${tierSpendingRuleId}`,
                        "name": "spending camp",
                        "startDate": "2019-01-02T14:57:19.000Z",
                        "endDate": "2020-02-01T14:57:19.000Z",
                        "bannerUrl": null,
                        "terms": null,
                        "fixedPointSpent": 5,
                        "globalDailyLimit": 3,
                        "createdAt": "2019-12-04T07:46:52.000Z",
                        "updatedAt": "2019-12-04T07:46:52.000Z"
                    
    
            },
            "user_id":"testUser1"
            
            })
            .end((err,res)=>{
                res.should.have.status(400);
                
                res.body.debugError.should.equal("You have used this promotion 2")


                   
                done()
        });
        });
    });

    describe('testing canceling a point transaction',()=>{

        it('should return canceled point transaction',(done)=>{
            chai.request(app)
            .post(`/api/v1/point-transactions/cancel/${pointTransId}`)
            .set('Content-type','application/json')
            .set('authorization',token)
            .end((err,res)=>{
              
                res.should.have.status(200);
                res.type.should.equal('application/json');
                res.body.data.pointTransaction.should.be.a('object');
                res.body.data.pointTransaction.should.include.keys(
                    'id',
                    'pointAccountId',
                    'spendingCampaignId',
                    'reference',
                    'creditAmount',
                    'status',
                    'pointTransactionTypeId'
                );
                res.body.data.pointTransaction.status.should.equal('canceled')
                done()
        });
        });

    });

    describe('testing making point transaction status success  ',()=>{

    it('should return point transaction ',(done)=>{
        chai.request(app)
        .post(`/api/v1/point-transactions/success/${pointTransId2}`)
        .set('Content-type','application/json')
        .set('authorization',token)
        .end((err,res)=>{
            res.should.have.status(200);
                res.type.should.equal('application/json');
                res.body.data.pointTransaction.should.be.a('object');
                res.body.data.pointTransaction.should.include.keys(
                    'id',
                    'pointAccountId',
                    'spendingCampaignId',
                    'reference',
                    'creditAmount',
                    'status',
                    'pointTransactionTypeId'
                );
                res.body.data.pointTransaction.status.should.equal('success')
                
            done()
    });
    });
 

    });
    describe('testing account balance',()=>{

        it('should return users balance ',(done)=>{
          chai.request(app)
          .get('/api/v1/point-accounts/?company_id=compny1&user_id=testUser1')
          .set('Content-type','application/json')
          .set('authorization',token)
          .end((err,res)=>{
              res.should.have.status(200);
              res.type.should.equal('application/json');
              res.body.data.pointAccounts.should.be.a('object');
              let deductedBalance = balance - 15
              res.body.data.pointAccounts.balance.should.equal(deductedBalance);
              done()
                 });
          });
      });

      describe('testing point consiladtion ',()=>{

        it('should create point conslidation',(done)=>{
          chai.request(app)
          .post(`/api/v1/point-transactions/consolidation/${pointAccountId}`)
          .set('Content-type','application/json')
          .set('authorization',token)
          .end((err,res)=>{
              res.should.have.status(200);
              res.type.should.equal('application/json');
              res.body.data.pointConsiladation.should.be.an('array');
              res.body.data.pointConsiladation.forEach(element => {
                  element.should.include.keys(
                    "id",
                    "pointAccountId",
                    "pointTransactionFromId",
                    "pointTransactionToId",
                  )
                  element.pointAccountId.should.equal(pointAccountId)
                  element.pointTransactionFromId.should.not.be.empty
                  element.pointTransactionToId.should.not.be.empty

              });
            
              done()
                 });
          });
      });
  
      describe('testing point consiladtion ',()=>{

          it('should return users balance equal to zero ',(done)=>{
            chai.request(app)
            .get('/api/v1/point-accounts/?company_id=compny1&user_id=testUser2')
            .set('Content-type','application/json')
            .set('authorization',token)
            .end((err,res)=>{
                res.should.have.status(200);
                res.type.should.equal('application/json');
                res.body.data.pointAccounts.should.be.a('object');
                res.body.data.pointAccounts.balance.should.equal(0);
                done()
                   });
            });
       
      });
      describe('testing point consiladtion ',()=>{

    
            it('should create point conslidation',(done)=>{
                chai.request(app)
                .post(`/api/v1/point-transactions/consolidation/${account2}`)
                .set('Content-type','application/json')
                .set('authorization',token)
                .end((err,res)=>{
                    res.should.have.status(400);
                    res.should.have.status(400);
                
                    res.body.debugError.should.startsWith('this account is not ')
    
    
                       
                    done()
                  
                       });
                });
      });
  
});
