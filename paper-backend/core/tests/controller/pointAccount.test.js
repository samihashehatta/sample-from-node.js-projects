process.env.NODE_ENV= 'test';
const chai= require('chai');
const chaiHttp=require('chai-http');
const app = require('../../server');
// const assert = chai.assert;
chai.use(chaiHttp);
chai.should()
describe('testing point accounts routs ',()=>{
let token;
    before((done)=>{
            
            chai.request(app)
            .post('/api/v1/user/login')
            .set('Content-type','application/json')
            .send({
            
                "email":"samiha@gmail.com",
                "password":"sa1223"
            
            })
            .end((err,res)=>{
                token=res.body.data.token
                done()
        });

        });

    it('should add new point account',(done)=>{
            
            chai.request(app)
            .post('/api/v1/point-accounts/')
            .set('Content-type','application/json')
            .set('authorization',token)
            .send({
            
                "company_id":"compny1",
                "user_id":"user4",
                "is_owner":false,
                "verified":false
            
            })
            .end((err,res)=>{

                res.should.have.status(201);
                res.type.should.equal('application/json');
                res.body.should.be.a('object');
                res.error.should.equal(false);
    
                res.body.data.pointAccount.should.include.keys(
                        'id',
                        'companyId',
                        'isOwner',
                        'userId',
                        'verified',
                        'verifiedAt',
                        'updatedAt',
                        'createdAt'
                 )
                 
                done()
            });
    });
      it('should add new point account',(done)=>{
            
            chai.request(app)
            .post('/api/v1/point-accounts/')
            .set('Content-type','application/json')
            .set('authorization',token)
            .send({
            
                "company_id":"compny1",
                "user_id":"user5",
                "is_owner":false,
                "verified":false
            
            })
            .end((err,res)=>{

                res.should.have.status(201);
                res.type.should.equal('application/json');
                res.body.should.be.a('object');
                res.error.should.equal(false);
    
                res.body.data.pointAccount.should.include.keys(
                        'id',
                        'companyId',
                        'isOwner',
                        'userId',
                        'verified',
                        'verifiedAt',
                        'updatedAt',
                        'createdAt'
                 )
                 
                done()
            });
    });
    it('Make a point account owner',(done)=>{
            
        chai.request(app)
        .put('/api/v1/point-accounts/user4')
        .set('Content-type','application/json')
        .set('authorization',token)
        .end((err,res)=>{

            res.should.have.status(200);
            res.type.should.equal('application/json');
            res.body.should.be.a('object');
            res.error.should.equal(false);

            res.body.data.pointAccount.should.include.keys(
                    'id',
                    'companyId',
                    'isOwner',
                    'userId',
                    'verified',
                    'verifiedAt',
                    'updatedAt',
                    'createdAt'
             )
             res.body.data.pointAccount.isOwner.should.be.true
            done()
        });
    });
    it('Should not make a point account owner',(done)=>{
            
        chai.request(app)
        .put('/api/v1/point-accounts/user5')
        .set('Content-type','application/json')
        .set('authorization',token)
        .end((err,res)=>{

            res.should.have.status(400);
            res.type.should.equal('application/json');
            res.body.should.be.a('object');
            res.body.debugError.should.equal('user can not be owner ',)
            done()
        });
    });
    it('Should verify point account ',(done)=>{
            
        chai.request(app)
        .post('/api/v1/point-accounts/verify/user4')
        .set('Content-type','application/json')
        .set('authorization',token)
        .end((err,res)=>{
            res.should.have.status(200);
            res.type.should.equal('application/json');
            res.body.should.be.a('object');
            res.error.should.equal(false);
            res.body.data.pointAccount.should.include.keys(
                'id',
                'companyId',
                'isOwner',
                'userId',
                'verified',
                'verifiedAt',
                'updatedAt',
                'createdAt'
                )
            res.body.data.pointAccount.verified.should.be.true
            res.body.data.pointAccount.verifiedAt.should.not.be.empty
            done()
        });
    });
    it('Should not verify point account ',(done)=>{
            
        chai.request(app)
        .post('/api/v1/point-accounts/verify/user4')
        .set('Content-type','application/json')
        .set('authorization',token)
        .end((err,res)=>{
            res.should.have.status(400);
            res.type.should.equal('application/json');
            res.body.should.be.a('object');
            res.body.debugError.should.equal("this user is already verified")
            done()
        });
    });
});
