process.env.NODE_ENV= 'test';
const chai= require('chai');
const chaiHttp=require('chai-http');
const app = require('../../server');
const companyTierRepo =require('../../system/repositories/companyTierRepo')
const loyaltyTierRepo =require('../../system/repositories/loyaltyTierRepo')
const spendingRuleRepo =require('../../system/repositories/spendingRuleRepo')
const loyalityTierSpendingRuleRepo =require('../../system/repositories/loyalityTierSpendingRuleRepo')
const spendingCampaignRepo =require('../../system/repositories/spendingCampaignRepo')
const voucherRepo =require('../../system/repositories/voucherRepo')

const promotionService =  require('../../system/services/business/promotionService')
chai.use(require('chai-string'));
// const assert = chai.assert;
chai.use(chaiHttp);
chai.should()

var tierSpendingRuleId,loyalityTierId,spendingRuleId,token,spendingRuleId2;
var camps =[] , vouhcers = [];

const testData = async() =>{

    let body = {code:"TEXT12",name:"hello man122",point_threshold:5000}
    const createLoyaltyTier = await loyaltyTierRepo.create(body)
    const LoyaltyTier = createLoyaltyTier

    body = {name:'camp',
        companyId:"compny1",loyalityTierId:`${LoyaltyTier.id}`,startDate:new Date('2019-12-02'),
        endDate:new Date('2020-01-01')}
    const companyTier = await companyTierRepo.createCompanyLoyaltyTier(body)

    body ={    code:"TEXT123",
    name:"hello man122",
    event_type:"oneTime",
    event_status:"active"}
    const spendingRule = await spendingRuleRepo.create(body)
    body ={    code:"TEvoucher23",
    name:"hello voucher",
    event_type:"oneTime",
    event_status:"active"}
    const spendingRule2 = await spendingRuleRepo.create(body)
    spendingRuleId2 = spendingRule2.id
    spendingRuleId=spendingRule.id
    loyalityTierId=LoyaltyTier.id

    body= {loyalityTierId:LoyaltyTier.id,
    spendingRuleId:spendingRule.id,
    currency:"IDR",
    fixedPointDiscount:50000,
    minTransactionValue:10000,
    periodicLimit:5,
    periodicLimitLength:7,}
    const loyalityTierSpendingRule  = await loyalityTierSpendingRuleRepo.createLoyaltyTierSpendingRule(body)

    tierSpendingRuleId = loyalityTierSpendingRule.id

    const obj = [{
        tierSpendingRuleId:loyalityTierSpendingRule.id,
        name:"spending camp",
        fixedPointSpent:5,
        globalDailyLimit:3,
        startDate:"2019-01-02T14:57:19.117Z",
        endDate:"2020-02-01T14:57:19.117Z"},
        {	    
        tierSpendingRuleId:loyalityTierSpendingRule.id,
        name:"spending camp1",
        fixedPointSpent:5,
        globalDailyLimit:3,
        startDate:"2020-01-01T14:57:19.117Z",
        endDate:"2020-02-01T14:57:19.117Z"}
        ,{	    
        tierSpendingRuleId:loyalityTierSpendingRule.id,
        name:"spending camp2",
        fixedPointSpent:5,
        globalDailyLimit:3,
        startDate:"2019-01-01T14:57:19.117Z",
        endDate:"2019-02-02T14:57:19.117Z"}]
    const spendingCampaigns = await spendingCampaignRepo.createSpendingCampaigns(obj)
    spendingCampaigns.map((spendingCampaign)=>{
        if(spendingCampaign.name=="spending camp")
        camps[0] = spendingCampaign.id
        else if(spendingCampaign.name=="spending camp1")
        camps[1] = spendingCampaign.id
        else if(spendingCampaign.name=="spending camp2")
        camps[2] = spendingCampaign.id
    })
    body = [
        
        {"spendingRuleId": spendingRule2.id,
        "startDate": "2019-12-11T14:57:19.117Z",
        "expiryDate": "2020-02-01T14:57:19.117Z",
        "currency": "IDR",
        "bannerUrl": null,
        "terms": null,
        "fixedVoucherAmount": null,
        "pctVoucherDicount": 25,
        "minTransactionValue": null,
        "periodicLimit": null,
        "periodicLimitLength": null,
        "monthlyLimit": 50,
        "globalDailyLimit": null,
        "maxLimit": 10,},    
    {"spendingRuleId": spendingRuleId,
    "startDate": "2019-12-14T14:57:19.117Z",
    "expiryDate": "2020-02-01T14:57:19.117Z",
    "currency": "IDR",
    "bannerUrl": null,
    "terms": null,
    "fixedVoucherAmount": null,
    "pctVoucherDicount": 10,
    "minTransactionValue": 1000000,
    "periodicLimit": 5,
    "periodicLimitLength": 10,
    "monthlyLimit": 6,
    "globalDailyLimit":2,
    "maxLimit": 10,},
{
    "spendingRuleId": spendingRuleId,
            "startDate": "2019-01-01T14:57:19.117Z",
            "expiryDate": "2019-02-02T14:57:19.117Z",
            "currency": "IDR",
            "bannerUrl": null,
            "terms": null,
            "fixedVoucherAmount": null,
            "pctVoucherDicount": 5,
            "minTransactionValue": null,
            "periodicLimit": null,
            "periodicLimitLength": null,
            "monthlyLimit": 50,
            "globalDailyLimit": null,
            "maxLimit": 10,
}]
    const vouchers =  await voucherRepo.createVouchers(body)
    vouchers.map((voucher)=>{
        if(voucher.pctVoucherDicount==5)
        vouhcers[0] = vouhcer.id
        else if(voucher.pctVoucherDicount==10)
        vouhcers[1] = vouhcer.id
        else if(voucher.pctVoucherDicount==25)
        vouhcers[2] =vouhcer.id
    })
};


describe('testing promotions ',()=>{
    before((done)=>{
            
        chai.request(app)
        .post('/user/login')
        .set('Content-type','application/json')
        .send({
        
            "email":"samiha@gmail.com",
            "password":"sa1223"
        
        })
        .end((err,res)=>{
            token=res.body.data.token
            done()
    });

    });

    describe('creating testing data ',()=>{

    it('should make test data',(done)=>{
        testData()
        .then(companyTier=>{
            done()
        })
    });
    });

    describe('test users company id ',()=>{

    it('should return user campanytierId',(done)=>{
        promotionService.getLoyaltyTierIdViaUserId('user4')
        .then(companyTier=>{
            companyTier.loyalityTierId.should.equal(loyalityTierId)
            done()
        })
    });
    });

    describe('testing if returned promotions has the right loyalty spending rule ',()=>{

    it('should return all spending capmign related to user',(done)=>{
        chai.request(app)
        .get('/api/v1/promotions/')
        .set('Content-type','application/json')
        .set('authorization',token)
        .send({
        
            "user_id":"user4",
            "spending_rule_id":`${spendingRuleId}`,
            "currency":"IDR",
            "amount":20000
        
        })
        .end((err,res)=>{
            res.should.have.status(200);
            res.type.should.equal('application/json');
            res.body.data.promotions.spendingCampaign.should.be.a('array');
            res.body.data.promotions.vouchers.should.be.a('array');

            res.body.data.promotions.spendingCampaign.forEach(element => {
                element.tierSpendingRuleId.should.equal(tierSpendingRuleId)
            });
            res.body.data.promotions.vouchers.length.should.equal(2);

            done()
    });
    });
    it('should return all vouchers related to user',(done)=>{
        chai.request(app)
        .get('/api/v1/promotions/')
        .set('Content-type','application/json')
        .set('authorization',token)
        .send({
        
            "user_id":"user4",
            "spending_rule_id":`${spendingRuleId}`,
            "currency":"IDR",
            "amount":20000
        
        })
        .end((err,res)=>{
            res.should.have.status(200);
            res.type.should.equal('application/json');
            res.body.data.promotions.spendingCampaign.should.be.a('array');
            res.body.data.promotions.vouchers.should.be.a('array');

            res.body.data.promotions.vouchers.forEach(element => {
                element.spendingRuleId.should.equal(spendingRuleId)
            });
            res.body.data.promotions.vouchers.length.should.equal(2);

            done()
    });
    });
    });

    describe('testing expiration date on promotions ',()=>{

      it('should return an error message for this spending campaign',(done)=>{
        chai.request(app)
        .get('/api/v1/promotions/validate')
        .set('Content-type','application/json')
        .set('authorization',token)
        .send({
        
            "spending_campaign":{
                
                    "id": `${camps[2]}`,
                    "tierSpendingRuleId": `${tierSpendingRuleId}`,
                    "name": "spending camp2",
                    "startDate": "2019-01-01T14:57:19.000Z",
                    "endDate": "2019-02-02T14:57:19.000Z",
                    "bannerUrl": null,
                    "terms": null,
                    "fixedPointSpent": 5,
                    "globalDailyLimit": 3,
                    "createdAt": "2019-12-04T07:46:52.000Z",
                    "updatedAt": "2019-12-04T07:46:52.000Z"
                

            
            },
            "user_id":"user4"
        })
        .end((err,res)=>{
            res.should.have.status(400);
            res.type.should.equal('application/json');
            res.body.should.be.a('object');
            res.body.debugError.should.equal("This promotion has expired")
            done()
    });
        });
      it('should return an error message for this voucher',(done)=>{
        chai.request(app)
        .get('/api/v1/promotions/validate')
        .set('Content-type','application/json')
        .set('authorization',token)
        .send({
        
            "voucher": 
                {  
			"id": vouhcers[0],
            "spendingRuleId": spendingRuleId,
            "startDate": "2019-01-01T14:57:19.117Z",
            "expiryDate": "2019-02-02T14:57:19.117Z",
            "currency": "IDR",
            "bannerUrl": null,
            "terms": null,
            "fixedVoucherAmount": null,
            "pctVoucherDicount": 5,
            "minTransactionValue": null,
            "periodicLimit": null,
            "periodicLimitLength": null,
            "monthlyLimit": 50,
            "globalDailyLimit": null,
            "maxLimit": 10,
                },
            "user_id":"user4"
        })
        .end((err,res)=>{
            res.should.have.status(400);
            res.type.should.equal('application/json');
            res.body.should.be.a('object');
            res.body.debugError.should.equal("This promotion has expired")
            done()
    });
        });
    });

    describe('testing start date on promotions',()=>{

        it('should return an error message',(done)=>{
            chai.request(app)
            .get('/api/v1/promotions/validate')
            .set('Content-type','application/json')
            .set('authorization',token)
            .send({
                "spending_campaign":{
                
                        "id": `${camps[1]}`,
                        "tierSpendingRuleId": `${tierSpendingRuleId}`,
                        "name": "spending camp1",
                        "startDate": "2020-01-01T14:57:19.000Z",
                        "endDate": "2020-02-01T14:57:19.000Z",
                        "bannerUrl": null,
                        "terms": null,
                        "fixedPointSpent": 5,
                        "globalDailyLimit": 3,
                        "createdAt": "2019-12-04T07:46:52.000Z",
                        "updatedAt": "2019-12-04T07:46:52.000Z"
                    
                
            },
            "user_id":"user4"
            
            })
            .end((err,res)=>{
                res.should.have.status(400);
                res.type.should.equal('application/json');
                res.body.should.be.a('object');
                res.body.debugError.should.equal("This promotion hasn't started")
                done()
        });
        });
        it('should return an error message',(done)=>{
            chai.request(app)
            .get('/api/v1/promotions/validate')
            .set('Content-type','application/json')
            .set('authorization',token)
            .send({
                "voucher": 
                {  
            "id": `${vouhcers[1]}`,
            "spendingRuleId": `${spendingRuleId}`,
            "startDate": "2019-12-14T14:57:19.117Z",
            "expiryDate": "2020-02-01T14:57:19.117Z",
            "currency": "IDR",
            "bannerUrl": null,
            "terms": null,
            "fixedVoucherAmount": null,
            "pctVoucherDicount": 10,
            "minTransactionValue": null,
            "periodicLimit": null,
            "periodicLimitLength": null,
            "monthlyLimit": 50,
            "globalDailyLimit": null,
            "maxLimit": 10
            },
            "user_id":"user4"
            
            })
            .end((err,res)=>{
                res.should.have.status(400);
                res.type.should.equal('application/json');
                res.body.should.be.a('object');
                res.body.debugError.should.equal("This promotion hasn't started")
                done()
        });
        });

    });

    describe('testing spending campaing ',()=>{

    it('should return spending campaign ',(done)=>{
        chai.request(app)
        .get('/api/v1/promotions/validate')
        .set('Content-type','application/json')
        .set('authorization',token)
        .send({
            "spending_campaign":{
                
                    "id": `${camps[0]}`,
                    "tierSpendingRuleId": `${tierSpendingRuleId}`,
                    "name": "spending camp",
                    "startDate": "2019-01-02T14:57:19.000Z",
                    "endDate": "2020-02-01T14:57:19.000Z",
                    "bannerUrl": null,
                    "terms": null,
                    "fixedPointSpent": 5,
                    "globalDailyLimit": 3,
                    "createdAt": "2019-12-04T07:46:52.000Z",
                    "updatedAt": "2019-12-04T07:46:52.000Z"
                

        },
        "user_id":"user4"
        
        })
        .end((err,res)=>{
            res.should.have.status(200);
            res.type.should.equal('application/json');
            res.body.data.promotion.should.be.a('object');
            res.body.data.promotion.should.include.keys(
                "id",
                "tierSpendingRuleId",
                "name",
                "startDate",
                "endDate",
                "bannerUrl",
                "terms",
                "fixedPointSpent",
                "globalDailyLimit",
                "createdAt",
                "updatedAt",
            )
               
            done()
    });
    });
    it('should return voucher ',(done)=>{
        chai.request(app)
        .get('/api/v1/promotions/validate')
        .set('Content-type','application/json')
        .set('authorization',token)
        .send({
            "voucher":{
                
                    "id": `${vouhcers[2]}`,
                    "spendingRuleId": spendingRuleId2,
                    "startDate": "2019-12-11T14:57:19.117Z",
                    "expiryDate": "2020-02-01T14:57:19.117Z",
                    "currency": "IDR",
                    "bannerUrl": null,
                    "terms": null,
                    "fixedVoucherAmount": null,
                    "pctVoucherDicount": 25,
                    "minTransactionValue": null,
                    "periodicLimit": null,
                    "periodicLimitLength": null,
                    "monthlyLimit": 50,
                    "globalDailyLimit": null,
                    "maxLimit": 10
        },
        "user_id":"user4"
        
        })
        .end((err,res)=>{
            res.should.have.status(200);
            res.type.should.equal('application/json');
            res.body.data.promotion.should.be.a('object');
            res.body.data.promotion.should.include.keys(
                "id",
                "spendingRuleId",
                "startDate",
                "expiryDate",
                "currency",
                "bannerUrl",
                "terms",
                "fixedVoucherAmount",
                "pctVoucherDicount",
                "minTransactionValue",
                "periodicLimit",
                "periodicLimitLength",
                "monthlyLimit",
                "globalDailyLimit",
                "maxLimit"
            )
               
            done()
    });
    });

    });

    describe('testing Point balance in spending campaing ',()=>{

    it('should return an error message',(done)=>{
        chai.request(app)
        .get('/api/v1/promotions/validate')
        .set('Content-type','application/json')
        .set('authorization',token)
        .send({
            "spending_campaign":{
            
                    "id": `${camps[1]}`,
                    "tierSpendingRuleId": `${tierSpendingRuleId}`,
                    "name": "spending camp1",
                    "startDate": "2020-01-01T14:57:19.000Z",
                    "endDate": "2020-02-01T14:57:19.000Z",
                    "bannerUrl": null,
                    "terms": null,
                    "fixedPointSpent": 5,
                    "globalDailyLimit": 3,
                    "createdAt": "2019-12-04T07:46:52.000Z",
                    "updatedAt": "2019-12-04T07:46:52.000Z"
                
             
        },
        "user_id":"user4"
        
        })
        .end((err,res)=>{
            res.should.have.status(400);
            res.type.should.equal('application/json');
            res.body.should.be.a('object');
            res.body.debugError.should.equal( "You don't have enough balance in your points")
            done()
    });
    });

    });
    
    describe('testing min_transaction_value',()=>{

        it('should return an error message for this spending campaign',(done)=>{
            chai.request(app)
            .get('/api/v1/promotions/validate')
            .set('Content-type','application/json')
            .set('authorization',token)
            .send({
                "spending_campaign":{
                
                        "id": `${camps[1]}`,
                        "tierSpendingRuleId": `${tierSpendingRuleId}`,
                        "name": "spending camp1",
                        "startDate": "2020-01-01T14:57:19.000Z",
                        "endDate": "2020-02-01T14:57:19.000Z",
                        "bannerUrl": null,
                        "terms": null,
                        "fixedPointSpent": 5,
                        "globalDailyLimit": 3,
                        "createdAt": "2019-12-04T07:46:52.000Z",
                        "updatedAt": "2019-12-04T07:46:52.000Z"
                    
                
            },
            "user_id":"user4"
            
            })
            .end((err,res)=>{
                res.should.have.status(400);
                res.type.should.equal('application/json');
                res.body.should.be.a('object');
                res.body.debugError.should.startWith("You haven't reached the minimum transaction amount")
                done()
        });
        });
        it('should return an error message for this voucher',(done)=>{
            chai.request(app)
            .get('/api/v1/promotions/validate')
            .set('Content-type','application/json')
            .set('authorization',token)
            .send({
                "voucher": 
                {  
            "id": `${vouhcers[0]}`,
            "spendingRuleId":`${spendingRuleId}`,
            "startDate": "2019-12-14T14:57:19.117Z",
            "expiryDate": "2020-02-01T14:57:19.117Z",
            "currency": "IDR",
            "bannerUrl": null,
            "terms": null,
            "fixedVoucherAmount": null,
            "pctVoucherDicount": 10,
            "minTransactionValue": null,
            "periodicLimit": null,
            "periodicLimitLength": null,
            "monthlyLimit": 50,
            "globalDailyLimit": null,
            "maxLimit": 10
            },
            "user_id":"user4",
            "amount":100000
            
            })
            .end((err,res)=>{
                res.should.have.status(400);
                res.type.should.equal('application/json');
                res.body.should.be.a('object');
                res.body.debugError.should.startWith("You haven't reached the minimum transaction amount")
                done()
        });
        });

    });

    describe('testing periodic_limit and length',()=>{

        it('should return an error message for this spending campaign',(done)=>{
            chai.request(app)
            .get('/api/v1/promotions/validate')
            .set('Content-type','application/json')
            .set('authorization',token)
            .send({
                "spending_campaign":{
                
                        "id": `${camps[1]}`,
                        "tierSpendingRuleId": `${tierSpendingRuleId}`,
                        "name": "spending camp1",
                        "startDate": "2020-01-01T14:57:19.000Z",
                        "endDate": "2020-02-01T14:57:19.000Z",
                        "bannerUrl": null,
                        "terms": null,
                        "fixedPointSpent": 5,
                        "globalDailyLimit": 3,
                        "createdAt": "2019-12-04T07:46:52.000Z",
                        "updatedAt": "2019-12-04T07:46:52.000Z"
                    
                
            },
            "user_id":"user4"
            
            })
            .end((err,res)=>{
                res.should.have.status(400);
                res.type.should.equal('application/json');
                res.body.should.be.a('object');
                res.body.debugError.should.startWith("You have used this promotion")
                done()
        });
        });
        it('should return an error message for this voucher',(done)=>{
            chai.request(app)
            .get('/api/v1/promotions/validate')
            .set('Content-type','application/json')
            .set('authorization',token)
            .send({
                "voucher":{
                
                        "id": `${vouhcers[1]}`,
                        "spendingRuleId": spendingRuleId,
    "startDate": "2019-12-14T14:57:19.117Z",
    "expiryDate": "2020-02-01T14:57:19.117Z",
    "currency": "IDR",
    "bannerUrl": null,
    "terms": null,
    "fixedVoucherAmount": null,
    "pctVoucherDicount": 10,
    "minTransactionValue": 1000000,
    "periodicLimit": 5,
    "periodicLimitLength": 10,
    "monthlyLimit": 50,
    "globalDailyLimit": null,
    "maxLimit": 10
                    
                
            },
            "user_id":"user4"
            
            })
            .end((err,res)=>{
                res.should.have.status(400);
                res.type.should.equal('application/json');
                res.body.should.be.a('object');
                res.body.debugError.should.startWith("You have used this promotion")
                done()
        });
        });

    });

    describe('testing max_limit',()=>{

        it('should return an error message for this spending campaign',(done)=>{
            chai.request(app)
            .get('/api/v1/promotions/validate')
            .set('Content-type','application/json')
            .set('authorization',token)
            .send({
                "spending_campaign":{
                
                        "id": `${camps[1]}`,
                        "tierSpendingRuleId": `${tierSpendingRuleId}`,
                        "name": "spending camp1",
                        "startDate": "2020-01-01T14:57:19.000Z",
                        "endDate": "2020-02-01T14:57:19.000Z",
                        "bannerUrl": null,
                        "terms": null,
                        "fixedPointSpent": 5,
                        "globalDailyLimit": 3,
                        "createdAt": "2019-12-04T07:46:52.000Z",
                        "updatedAt": "2019-12-04T07:46:52.000Z"
                    
                 
            },
            "user_id":"user4"
            
            })
            .end((err,res)=>{
                res.should.have.status(400);
                res.type.should.equal('application/json');
                res.body.should.be.a('object');
                res.body.debugError.should.startWith("You have used this promotion")
                done()
        });
        });
        it('should return an error message for this voucher',(done)=>{
            chai.request(app)
            .get('/api/v1/promotions/validate')
            .set('Content-type','application/json')
            .set('authorization',token)
            .send({
                "voucher":{
                
                    "id": `${vouhcers[1]}`,
                    "spendingRuleId": spendingRuleId,
                    "startDate": "2019-12-14T14:57:19.117Z",
                    "expiryDate": "2020-02-01T14:57:19.117Z",
                    "currency": "IDR",
                    "bannerUrl": null,
                    "terms": null,
                    "fixedVoucherAmount": null,
                    "pctVoucherDicount": 10,
                    "minTransactionValue": 1000000,
                    "periodicLimit": 5,
                    "periodicLimitLength": 10,
                    "monthlyLimit": 50,
                    "globalDailyLimit": null,
                    "maxLimit": 10
                 
            },
            "user_id":"user4"
            
            })
            .end((err,res)=>{
                res.should.have.status(400);
                res.type.should.equal('application/json');
                res.body.should.be.a('object');
                res.body.debugError.should.startWith("You have used this promotion")
                done()
        });
        });
    });

    describe('testing monthly_limit ',()=>{

        it('should return an error message for this spending campaign',(done)=>{
            chai.request(app)
            .get('/api/v1/promotions/validate')
            .set('Content-type','application/json')
            .set('authorization',token)
            .send({
                "spending_campaign":{
                
                        "id": `${camps[1]}`,
                        "tierSpendingRuleId": `${tierSpendingRuleId}`,
                        "name": "spending camp1",
                        "startDate": "2020-01-01T14:57:19.000Z",
                        "endDate": "2020-02-01T14:57:19.000Z",
                        "bannerUrl": null,
                        "terms": null,
                        "fixedPointSpent": 5,
                        "globalDailyLimit": 3,
                        "createdAt": "2019-12-04T07:46:52.000Z",
                        "updatedAt": "2019-12-04T07:46:52.000Z"
                    
                 
            },
            "user_id":"user4"
            
            })
            .end((err,res)=>{
                res.should.have.status(400);
                res.type.should.equal('application/json');
                res.body.should.be.a('object');
                res.body.debugError.should.startWith("You have used this promotion")
                done()
        });
        });
        it('should return an error message for this voucher',(done)=>{
            chai.request(app)
            .get('/api/v1/promotions/validate')
            .set('Content-type','application/json')
            .set('authorization',token)
            .send({
                "vocher":{
                
                        "id": `${vouhcers[1]}`,
                        "spendingRuleId": spendingRuleId,
                        "startDate": "2019-12-14T14:57:19.117Z",
                        "expiryDate": "2020-02-01T14:57:19.117Z",
                        "currency": "IDR",
                        "bannerUrl": null,
                        "terms": null,
                        "fixedVoucherAmount": null,
                        "pctVoucherDicount": 10,
                        "minTransactionValue": 1000000,
                        "periodicLimit": 5,
                        "periodicLimitLength": 10,
                        "monthlyLimit": 6,
                        "globalDailyLimit": null,
                        "maxLimit": 10
                    
                 
            },
            "user_id":"user4"
            
            })
            .end((err,res)=>{
                res.should.have.status(400);
                res.type.should.equal('application/json');
                res.body.should.be.a('object');
                res.body.debugError.should.startWith("You have used this promotion")
                done()
        });
        });
    
    });
    
    describe('testing global_daily_limit   ',()=>{

        it('should return an error message for this spending campaign',(done)=>{
            chai.request(app)
            .get('/api/v1/promotions/validate')
            .set('Content-type','application/json')
            .set('authorization',token)
            .send({
                "spending_campaign":{
                
                        "id": `${camps[1]}`,
                        "tierSpendingRuleId": `${tierSpendingRuleId}`,
                        "name": "spending camp1",
                        "startDate": "2020-01-01T14:57:19.000Z",
                        "endDate": "2020-02-01T14:57:19.000Z",
                        "bannerUrl": null,
                        "terms": null,
                        "fixedPointSpent": 5,
                        "globalDailyLimit": 3,
                        "createdAt": "2019-12-04T07:46:52.000Z",
                        "updatedAt": "2019-12-04T07:46:52.000Z"
                    
                 
            },
            "user_id":"user4"
            
            })
            .end((err,res)=>{
                res.should.have.status(400);
                res.type.should.equal('application/json');
                res.body.should.be.a('object');
                res.body.debugError.should.equal("The daily limit of this promotion is finished! Please check again tomorrow")
                done()
        });
        });
        it('should return an error message for this voucher',(done)=>{
            chai.request(app)
            .get('/api/v1/promotions/validate')
            .set('Content-type','application/json')
            .set('authorization',token)
            .send({
                "voucher":{
                
                    "id": `${vouhcers[1]}`,
                    "spendingRuleId": spendingRuleId,
                    "startDate": "2019-12-14T14:57:19.117Z",
                    "expiryDate": "2020-02-01T14:57:19.117Z",
                    "currency": "IDR",
                    "bannerUrl": null,
                    "terms": null,
                    "fixedVoucherAmount": null,
                    "pctVoucherDicount": 10,
                    "minTransactionValue": 1000000,
                    "periodicLimit": 5,
                    "periodicLimitLength": 10,
                    "monthlyLimit": 6,
                    "globalDailyLimit": 2,
                    "maxLimit": 10
                    
                 
            },
            "user_id":"user4"
            
            })
            .end((err,res)=>{
                res.should.have.status(400);
                res.type.should.equal('application/json');
                res.body.should.be.a('object');
                res.body.debugError.should.equal("The daily limit of this promotion is finished! Please check again tomorrow")
                done()
        });
        });
    
    });
    describe('testing voucher returned with promotion ',()=>{

        
        it('should return 2 vouchers',(done)=>{
            chai.request(app)
            .get('/api/v1/promotions/')
            .set('Content-type','application/json')
            .set('authorization',token)
            .send({
            
                "user_id":"user4",
                "spending_rule_id":`${spendingRuleId}`,
                "currency":"IDR",
                "amount":20000
            
            })
            .end((err,res)=>{
                res.should.have.status(200);
                res.type.should.equal('application/json');
                res.body.data.promotions.spendingCampaign.should.be.a('array');
                res.body.data.promotions.vouchers.should.be.a('array');

                res.body.data.promotions.vouchers.length.should.equal(2);
    
                done()
        });
        });
    });
});
